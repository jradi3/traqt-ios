//
//  TimerLimitViewCell.swift
//  Traqt
//
//  Created by Tarek Abdala Rfaei Jradi on 9/19/14.
//  Copyright (c) 2014 Syligo. All rights reserved.
//

import Foundation

class TimerLimitViewCell: UITableViewCell {
    
    var circularSlider : CircularSliderInputView!
    
    required init(coder aDecoder: NSCoder) {
        circularSlider = CircularSliderInputView()
        super.init(coder: aDecoder)
    }
    
    override var inputView: UIView {
        return circularSlider.view
    }
    
    override func canBecomeFirstResponder() -> Bool {
        return true
    }
    
}