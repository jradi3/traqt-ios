//
//  SessionViewController.swift
//  Traqt
//
//  Created by Rafael Veronezi on 8/24/14.
//  Copyright (c) 2014 Syligo. All rights reserved.
//

import Foundation
import UIKit
import AudioToolbox
import ChronometerKit

class SessionViewController: UIViewController {

    //
    // MARK: - Propriedades
    
    var sessionTrack: SessionTrack?
    var activityId: String?
    var panelsHidden = false
    
    var tapRecognizer: UITapGestureRecognizer!
    var commandsRecognizer: UIPanGestureRecognizer!
    var hidePanelsRecognizer: UISwipeGestureRecognizer!
    
    lazy var endPracticeSound: SystemSoundID = {
        let endPracticeSoundPath = NSBundle.mainBundle().pathForResource("TibetanBell", ofType: "caf")!
        let endPracticeSoundURL = NSURL(fileURLWithPath: endPracticeSoundPath)
        var soundId: SystemSoundID = 0
        AudioServicesCreateSystemSoundID(endPracticeSoundURL, &soundId)
        return soundId
    }()
    
    //
    // MARK: - Outlets
    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var repetitionsLabel: UILabel!
    @IBOutlet weak var totalRepetitionsLabel: UILabel!
    @IBOutlet weak var repetitionsProgress: UIProgressView!
    @IBOutlet weak var titleActivity: UILabel!

    @IBOutlet var panels: [UIView]!
    @IBOutlet weak var showActionsLabel: UILabel!
    @IBOutlet weak var titlePanel: UIView!
    @IBOutlet weak var chronometerPanel: UIView!
    @IBOutlet weak var repetitionsPanel: UIView!
    @IBOutlet weak var actionsPanel: UIView!
    @IBOutlet weak var commandsLabelCenterConstraint: NSLayoutConstraint!
    
    //
    // MARK: - Métodos de apoio
    
    func configureGestureRecognizers() {
        // Configura o gesto de Taps para receber contabilizar as repetições com os toques na tela
        self.tapRecognizer = UITapGestureRecognizer(target: self, action: "tapGestureDetected:")
        self.tapRecognizer.numberOfTapsRequired = 1
        self.view.addGestureRecognizer(self.tapRecognizer)
        
        // Configura o gesto de Swipe da direta para a esquerda para mostrar o Action Sheet de comandos
        self.commandsRecognizer = UIPanGestureRecognizer(target: self, action: "commandsGestureDetected:")
        self.actionsPanel.addGestureRecognizer(self.commandsRecognizer)
    }
    
    func updateUI() {
        if let session = self.sessionTrack {
            self.repetitionsLabel.text = "\(session.currentRepetitions)"
            self.repetitionsProgress.progress = session.repetitionsProgress
        }
    }

    func presentActions() {
        let commandsSheet = UIAlertController(title: "Atividade em Andamento", message: "Comandos disponíveis para a sessão ativa.", preferredStyle: .ActionSheet)

        if self.sessionTrack?.sessionState == SessionTrack.SessionState.Running {
            let actionPause = UIAlertAction(title: "Pausar", style: .Default) { [weak session = self.sessionTrack] (alert) in session!.pauseSession() }
            commandsSheet.addAction(actionPause)
        } else if self.sessionTrack?.sessionState == SessionTrack.SessionState.Paused {
            let actionResume = UIAlertAction(title: "Retomar", style: .Default) { [weak session = self.sessionTrack] (alert) in session!.resumeSession() }
            commandsSheet.addAction(actionResume)
        }

        if self.sessionTrack!.activity.measureRepetitions.boolValue && UIDevice.currentDevice().userInterfaceIdiom != UIUserInterfaceIdiom.Pad {
            let title = sessionTrack!.enableVibration ? "Desabilitar Vibração" : "Habilitar Vibração"
            commandsSheet.addAction(UIAlertAction(title: title, style: .Default) { (alert) in self.sessionTrack!.enableVibration = !self.sessionTrack!.enableVibration })
        }
        
        commandsSheet.addAction(UIAlertAction(title: "Parar Sessão", style: .Destructive) { [weak session = self.sessionTrack] (alert) in session!.cancelSession() })
        commandsSheet.addAction(UIAlertAction(title: "Cancelar", style: .Cancel, handler: nil))

        if UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad {
            commandsSheet.popoverPresentationController!.sourceView = self.actionsPanel
        }
        dispatch_async(dispatch_get_main_queue()) {
            self.presentViewController(commandsSheet, animated: true, completion: nil)
        }
    }
    
    //
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Configure gestures
        configureGestureRecognizers()

        // Configura a sessão
        if let actId = activityId {
            sessionTrack = SessionTrack(activityId: actId)
            sessionTrack!.updateTimerBlock = {
                (elapsedTime, remainingTime) in
                
                if remainingTime != nil {
                    self.timeLabel.text = "\(remainingTime!.getFormattedInterval(miliseconds: false))"
                } else {
                    self.timeLabel.text = "\(elapsedTime.getFormattedInterval(miliseconds: false))"
                }
                
            }
            sessionTrack!.completionBlock = {
                // Alerta o usuário da conclusão
                var alertC = UIAlertController(title: "Traqt", message: "Sessão finalizada!", preferredStyle: .Alert)
                alertC.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) { (alert: UIAlertAction!) -> Void in
                    self.dismissViewControllerAnimated(true, completion: nil)
                    })
                
                self.presentViewController(alertC, animated: true, completion: nil)
                AudioServicesPlaySystemSound(self.endPracticeSound)
            }
            
            // Configure aspect
            self.titleActivity.text = sessionTrack!.activity.name
            
            let prefix = sessionTrack!.activity.repetitions.integerValue > 0 ? "\(sessionTrack!.activity.repetitions)" : "∞"
            totalRepetitionsLabel.text = "\(prefix) repetições"
            
            if !sessionTrack!.activity.measureRepetitions.boolValue {
                repetitionsPanel.hidden = true
            }
            
            if sessionTrack!.activity.measureTime.boolValue && sessionTrack!.activity.timeLimit.integerValue > 0 {
                self.timeLabel.text = "\(NSTimeInterval(sessionTrack!.activity.timeLimit.floatValue).getFormattedInterval(miliseconds: false))"
            }
            
            sessionTrack!.startSession()
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        // Mantem a Interface sincronizada com o modelo
        self.updateUI()
        
        // Inibe o dispositivo de apagar a tela automaticamente
        UIApplication.sharedApplication().idleTimerDisabled = true
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Re-inibe o desligamento automatico da tela
        UIApplication.sharedApplication().idleTimerDisabled = false
    }

    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    //
    // MARK: - Selectors dos Reconhecedores de Gestos (Gesture Recognizers)
    
    /// Esse método é chamado quando o usuário realiza o gesto de toque (tap)
    func tapGestureDetected(sender: UITapGestureRecognizer) {
        if sessionTrack!.activity.measureRepetitions.boolValue {
            if sessionTrack!.enableVibration {
                AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            }
            
            sessionTrack?.addRepetition()
            updateUI()
        }
    }
    
    /// Esse método é chamado quando o usuário realiza o gesto de deslize dos extremos da direita para a esquerda da tela
    func commandsGestureDetected(gesture: UIScreenEdgePanGestureRecognizer) {
        if gesture.state == .Began || gesture.state == .Changed {
            var translation = gesture.translationInView(gesture.view!)
            self.commandsLabelCenterConstraint.constant = -translation.x
            
            // Show actions if vertical deslocation is greater than 60% of the screen size
            if translation.x / self.view.frame.size.width >= 0.6 {
                presentActions()
            }
        } else {
            self.commandsLabelCenterConstraint.constant = 0
            self.view.setNeedsUpdateConstraints()
            UIView.animateWithDuration(0.5) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func hidePanelsGestureDetected(sender: UIScreenEdgePanGestureRecognizer) {
        panelsHidden = !panelsHidden
        UIView.animateWithDuration(1.0) {
            for panel in self.panels {
                panel.hidden = self.panelsHidden
                panel.alpha = self.panelsHidden ? 0.1 : 1.0
            }
        }
    }
    
}