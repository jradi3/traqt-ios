//
//  ActivityFormViewController.swift
//  Traqt
//
//  Created by Rafael Veronezi on 8/15/14.
//  Copyright (c) 2014 Syligo. All rights reserved.
//

import UIKit

/*
    MARK: - TODO: O que falta para concluir o formulário

    - Incluir um mecanismo de validação na entidade: colocar na entidade uma forma de validar se tudo que ela precisa para ser inserida ou atualizada esta feito. Isso pode ser feito através de uma classe base que inclua algum mecanismo padrão de validação. Esse mecanismo seria disparado quando o usuário tocar no botão para salvar as alterações do formulário. Verificar a lista de erros, apresentar o erro para o usuário e direcioná-lo adequadamente. Pensar num mecanismo padronizado para direcionar para o erro (por exemplo, associar o erro a um determinado tipo de controle e disparar o become first responder? Pensar em algo...)
    - Sincronizar os componentes de tela com os valores do objeto: esta faltando sincronizar o PickerView e o Slider com os valores que vem da classe
    - Salvar formulário: Persiste essa atividade no repositório e deixa ela disponível para selecionar como sessão
 */

class ActivityFormViewController: StaticDataTableViewController, UITextFieldDelegate, UITextViewDelegate, WeekDaysPickerControllerDelegate, CircularSliderInputViewDelegate {

    enum DataOperation {
        case Add
        case Edit
    }
    
    //
    // MARK: - Properties
    
    var tableViewSections = 7
    let dataContext = TraqtDataContext.sharedInstance
    var activity: Activity!
    var dataOperation: DataOperation = .Add
    var remindTableView: WeekDaysPickerController?
    var hours : Int = 0
    var minutes : Int = 0
    
    //
    // MARK: - Outlets
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var descriptionTextView: SAMTextView!
    @IBOutlet weak var measureRepetitionsSwitch: UISwitch!
    @IBOutlet weak var repetitionsLabel: UILabel!
    @IBOutlet weak var repetitionsSlider: UISlider!
    @IBOutlet weak var enableVibrationSwitch: UISwitch!
    @IBOutlet weak var measureTimeSwitch: UISwitch!
    @IBOutlet weak var timeLimitLabel: UILabel!
    @IBOutlet weak var daysOfWeekLabel: UILabel!
    @IBOutlet weak var enableCountdownSwitch: UISwitch!
    @IBOutlet weak var remindersSwitch: UISwitch!
    @IBOutlet weak var timeReminderLabel: UILabel!
    @IBOutlet weak var reminderDaysLabel: UILabel!
    @IBOutlet weak var reminderTimePicker: UIDatePicker!
    @IBOutlet weak var lastSessionLabel: UILabel!

    @IBOutlet weak var repetitionsSliderCell: UITableViewCell!
    @IBOutlet weak var enableVibrationCell: UITableViewCell!
    @IBOutlet weak var timeReminderCell: UITableViewCell!
    @IBOutlet var repetitionsOptionsCells: [UITableViewCell]!
    @IBOutlet var timeLimitOptionsCells: [UITableViewCell]!
    @IBOutlet var reminderOptionsCells: [UITableViewCell]!
    @IBOutlet var lastActivityCells: [UITableViewCell]!
    
    //
    // MARK: - Method Overrides

    override func viewDidLoad() {
        super.viewDidLoad()

        // Configure the TableView
        self.hideSectionsWithHiddenRows = true
        self.insertTableViewRowAnimation = .Middle
        self.deleteTableViewRowAnimation = .Middle
        self.reloadTableViewRowAnimation = .Middle
        
        // Configure Views
        self.descriptionTextView.placeholder = "Descrição detalhada da atividade..."
        
        // Check if it's adding a new activity or editing a new one
        if activity == nil {
            activity = dataContext.activities.create()
            title = "Nova Atividade"
            dataOperation = .Add

            // Removes the last two sections that only makes sense on edit
            tableViewSections = 5
            tableView.reloadData()
        } else {
            title = "Editar Atividade"
            dataOperation = .Edit
        }
        
        // Sincroniza os dados do modelo com a interface
        self.syncModelToUI()
    }
    
    //
    // MARK: - Action Methods
    
    @IBAction func toggleRepetitionsSwitched(sender: UISwitch) {
        self.activity.measureRepetitions = sender.on
        self.toggleRepetitions(sender.on)
    }
    
    @IBAction func toggleEnableVibrationSwitch(sender: UISwitch) {
        self.activity.enableVibration = sender.on
    }
    
    @IBAction func repetitionsChanged(sender: UISlider) {
        self.activity.repetitions = Int(sender.value)
        syncRepetitionsLabel()
    }
    
    @IBAction func toggleTimeLimitSwitched(sender: UISwitch) {
        self.activity.measureTime = sender.on
        self.toggleTimeLimit(sender.on)
    }
    
    @IBAction func toggleRemindersSwitched(sender: UISwitch) {
        self.activity.reminders = sender.on
        self.toggleReminders(sender.on)
    }

    @IBAction func handleReminderDatePicker(sender : UIDatePicker){
        // Sync with model
        activity.reminderTime = sender.date.toIntTime()

        // Update UI
        syncReminderTimeLabel()
    }

    
    @IBAction func enableTimeReminder(indexPath: NSIndexPath) {
        if self.cellIsHidden(timeReminderCell){
            self.cell(timeReminderCell, setHidden: false)
            self.tableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Top, animated: true)
            self.reloadDataAnimated(true)
        } else {
            self.cell(timeReminderCell, setHidden: true)
            self.reloadDataAnimated(true)
        }
    }

    @IBAction func dismiss(sender: UIBarButtonItem) {
        // Verifica qual o tipo de operação e faz o tratamento adequado do cancelamento
        if self.dataOperation == .Add {
            // Inclusão: descarta o objeto criado
            self.dataContext.activities.delete(self.activity.activityId)
        } else {
            // Alteração: descarta as alterações feitas no objeto
            self.activity.managedObjectContext!.refreshObject(self.activity, mergeChanges: false)
        }
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    @IBAction func save(sender: UIBarButtonItem) {
        // Validation
        if nameTextField.text == nil || nameTextField.text.isEmpty {
            var alert = UIAlertController(title: "Traqt", message: "Você deve digitar o nome da atividade.", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) { (alert: UIAlertAction!) in
                let become = self.nameTextField.becomeFirstResponder()
            })
            
            self.presentViewController(alert, animated: true, completion: nil)
            return
        }
        
        // Process saving
        self.syncUIToModel()
        self.dataContext.saveContext()
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    //
    // MARK: - Support Methods
    
    func toggleRepetitions(enabled: Bool) {
        if enabled {
            // Check if it's iPad, so don't show the vibration option
            let hideVibration = UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad
            self.cell(self.enableVibrationCell, setHidden: hideVibration)
            
            self.cells(self.repetitionsOptionsCells, setHidden: false)
            self.reloadDataAnimated(true)
        } else {
            self.cells(self.repetitionsOptionsCells, setHidden: true)
            self.cell(self.enableVibrationCell, setHidden: true)
            self.reloadDataAnimated(true)
        }
    }
    
    func toggleTimeLimit(enabled: Bool) {
        if enabled {
            self.cells(timeLimitOptionsCells, setHidden: false)
            self.reloadDataAnimated(true)
        } else {
            self.cells(timeLimitOptionsCells, setHidden: true)
            self.reloadDataAnimated(true)
            self.resignFirstResponder()
        }

    }
    
    func toggleReminders(enabled: Bool) {
        self.cell(timeReminderCell, setHidden: true)
        if enabled {
            self.cells(reminderOptionsCells, setHidden: false)
            self.reloadDataAnimated(true)
        } else {
            self.cells(reminderOptionsCells, setHidden: true)
            self.reloadDataAnimated(true)
            self.resignFirstResponder()
        }
    }
    
    func toggleRepetitionSliderCell() {
        // TODO: Show/Hide the repetitions slider cell
        print("Toggled Repetitions Cell")
    }
    
    func toggleTimeLimitPickerCell(indexPath: NSIndexPath) {
        // TODO: Show/Hide the time limit picker cell
        print("Toggled Time Limit Cell")
        if !self.timeLimitOptionsCells[0].self.resignFirstResponder(){
            self.tableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Top, animated: true)
            let limit = self.timeLimitOptionsCells[0].self as TimerLimitViewCell
            limit.circularSlider.delegate = self
            self.timeLimitOptionsCells[0].becomeFirstResponder()
        }else{
            self.timeLimitOptionsCells[0].self.resignFirstResponder()
        }
    }
    
    /**
        Use esse método para manter os elementos da interface sincronizados com o modelo (atividade)
     */
    func syncModelToUI() {
        // Text inputs
        nameTextField.text = activity.name
        descriptionTextView.text = activity.details
        
        // Repetitions
        measureRepetitionsSwitch.on = activity.measureRepetitions.boolValue
        toggleRepetitions(activity.measureRepetitions.boolValue)
        repetitionsSlider.value = activity.repetitions.floatValue
        syncRepetitionsLabel()
        enableVibrationSwitch.on = activity.enableVibration.boolValue

        // Time Limit
        measureTimeSwitch.on = activity.measureTime.boolValue
        toggleTimeLimit(activity.measureTime.boolValue)
        
        let (h, m) = activity.timeLimitComponents()
        timeLimitLabel.text = "\(h) h, \(m) min."
        
        // Reminders
        remindersSwitch.on = activity.reminders.boolValue
        toggleReminders(activity.reminders.boolValue)
        syncReminderDaysLabel()
        reminderTimePicker.date = activity.reminderTime.integerValue.timeIntToDate()!
        syncReminderTimeLabel()
        
        // Last section
        let sessions = self.dataContext.sessions.count(NSPredicate(format: "%K == %@", "activity.activityId", self.activity.activityId))
        if sessions > 0 {
            self.lastSessionLabel.text = "\(sessions) " + ((sessions == 1) ? "Sessão" : "Sessões")
        }
    }
    
    func syncUIToModel() {
        self.activity.name = self.nameTextField.text
        self.activity.details = self.descriptionTextView.text
    }
    
    func syncRepetitionsLabel() {
        self.repetitionsLabel.text = "\(self.activity.repetitions)"
    }
    
    func syncReminderDaysLabel() {
        reminderDaysLabel.text = WeekDays(rawValue: activity.reminderDays.integerValue).toString()
    }
    
    func syncReminderTimeLabel() {
        var date = activity.reminderTime.integerValue.timeIntToDate()!
        var dateFormatter = NSDateFormatter()
        dateFormatter.timeStyle = NSDateFormatterStyle.ShortStyle
        timeReminderLabel.text = dateFormatter.stringFromDate(date)
    }
    
    //
    // MARK: - UIScrollViewDelegate
    
    override func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        self.view.endEditing(true)
    }
    
    //
    // MARK: - UITableViewDelegate
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return tableViewSections
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let indexPathT = (indexPath.section, indexPath.row)
        switch indexPathT {
        case (0, _):
            self.nameTextField.becomeFirstResponder()
            break
        case (1, _):
            self.descriptionTextView.becomeFirstResponder()
            self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
            break
        case (2, 1):
            self.toggleRepetitionSliderCell()
            self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
            break
        case (4, 2):
            self.enableTimeReminder(indexPath)
            break
        case (3, 1):
            self.toggleTimeLimitPickerCell(indexPath)
        case (6, 0):
            let alert = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
            alert.addAction(UIAlertAction(title: "Apagar Atividade", style: .Destructive) {
                (action) in
                self.dataContext.activities.delete(self.activity.activityId)
                self.dismissViewControllerAnimated(true, completion: nil)
            })
            alert.addAction(UIAlertAction(title: "Cancelar", style: .Cancel, handler: nil))
            
            self.presentViewController(alert, animated: true, completion: nil)
        default:
            break
        }
    }
        
    //
    // MARK: - WeekDaysPickerControllerDelegate
    
    func weekDaysPicker(controller: WeekDaysPickerController, didPickWeekDays weekdays: WeekDays) {
        activity.reminderDays = weekdays.rawValue
        syncReminderDaysLabel()
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func weekDaysPickerDidCancel(controller: WeekDaysPickerController) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    //
    // MARK: - CircularSliderInputViewDelegate
    
    func slideHoursDidChange(slider : EFCircularSlider) {
        self.hours = Int(slider.currentValue)
        timeLimitLabel.text = "\(self.hours) h, \(self.minutes) min."
    }
    
    func slideMinutesDidChange(slider : EFCircularSlider){
        self.minutes = Int(slider.currentValue)
        timeLimitLabel.text = "\(self.hours) h, \(self.minutes) min."
    }
    
    //
    // MARK: - Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if segue.identifier == "PickReminderDays" {
            let nvc = segue.destinationViewController as UINavigationController
            let vc = nvc.topViewController as WeekDaysPickerController
            vc.delegate = self
            vc.selectedWeekDays = WeekDays(rawValue: activity.reminderDays.integerValue)
        } else if segue.identifier == "ShowHistory" {
            let nvc = segue.destinationViewController as UINavigationController
            let vc = nvc.topViewController as HistoryTableViewController
            vc.activityId = self.activity!.activityId
        }
    }

}
