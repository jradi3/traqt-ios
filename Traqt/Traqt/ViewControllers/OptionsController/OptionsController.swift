//
//  SelectOptionsViewController.swift
//  Traqt
//
//  Created by Rafael Veronezi on 8/19/14.
//  Copyright (c) 2014 Syligo. All rights reserved.
//

import UIKit

typealias OnSelectedItemF = ((SelectableOption) -> ())?
typealias OnSelectionCancelledF = (() -> ())?

class OptionsController: UINavigationController {
    
    //
    // MARK: - Private Properties
    
    /// Uma conveniencia para obter o View Controller contido nesse controle de navegação
    private var optionsController: OptionsTableViewController? {
        return self.topViewController as? OptionsTableViewController
    }

    //
    // MARK: - Properties
    
    /// Título que será apresentado
    override var title: String? {
        get {
            return optionsController?.navigationItem.title
        }
        set {
            optionsController?.navigationItem.title = newValue
        }
    }
    
    /// Fonte de dados que será utilizada para alimentar a lista de opções
    /// A fonte de dados é um Array de objetos "SelectOptionItem"
    var dataSource: [SelectableOption] {
        get {
            return optionsController?.dataSource ?? []
        }
        set {
            optionsController?.dataSource = newValue
        }
    }

    /// Função que será executada quando um item for selecionado na lista
    var onSelectedItem: OnSelectedItemF {
        get {
            return optionsController?.onSelectedItem
        }
        set {
            optionsController?.onSelectedItem = newValue
        }
    }

    /// Função que será executada caso a seleção seja cancelada
    var onSelectionCancelled: OnSelectionCancelledF {
        get {
            return optionsController?.onSelectionCancelled
        }
        set {
            optionsController?.onSelectionCancelled = newValue
        }
    }
    
    //
    // MARK: - Class Methods
    
    class func optionsControllerWithItems(items: [SelectableOption]) -> OptionsController {
        var controller = UIStoryboard(name: "OptionsController", bundle: nil).instantiateViewControllerWithIdentifier("OptionsController") as OptionsController
        controller.dataSource = items
        return controller
    }
}
