//
//  SelectOptionItem.swift
//  Traqt
//
//  Created by Rafael Veronezi on 8/19/14.
//  Copyright (c) 2014 Syligo. All rights reserved.
//

import UIKit

private let kTableViewCellStyleDefaultIdentifier = "DefaultCell"
private let kTableViewCellStyleSubtitleIdentifier = "SubtitleCell"
private let kTableViewCellStyleValue1Identifier = "Value1Cell"
private let kTableViewCellStyleValue2Identifier = "Value2Cell"

struct SelectableOption {

    //
    // MARK: - Public Properties
    
    var sourceObject: Any?
    var title: String
    var subTitle: String?
    var isSelected: Bool = false
    var cellStyle: UITableViewCellStyle = UITableViewCellStyle.Default
    var cellIdentifier: String {
        switch cellStyle {
            case .Default:
                return kTableViewCellStyleDefaultIdentifier
            case .Subtitle:
                return kTableViewCellStyleSubtitleIdentifier
            case .Value1:
                return kTableViewCellStyleValue1Identifier
            case .Value2:
                return kTableViewCellStyleValue2Identifier
        }
    }
    var description: String {
        return "Titulo: \(self.title)\nObjeto de Origem: \(self.sourceObject?)"
    }
    
    //
    // MARK: - Initializers
    
    init(_ title: String) {
        self.title = title
    }
    
    init(_ title: String, subTitle: String, style: UITableViewCellStyle) {
        self.init(title)
        self.subTitle = subTitle
        self.cellStyle = style
    }
    
    //
    // MARK: - Class Methods
    
    static func generateOptionList<T>(items: [T], transformF: (T) -> (SelectableOption)) -> [SelectableOption] {
        var result = [SelectableOption]()
        for i in items {
            result.append(transformF(i))
        }
        return result
    }
    
}
