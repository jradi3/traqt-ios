//
//  SelectOptionsTableViewController.swift
//  Traqt
//
//  Created by Rafael Veronezi on 8/19/14.
//  Copyright (c) 2014 Syligo. All rights reserved.
//

import UIKit

class OptionsTableViewController: UITableViewController {
    
    //
    // MARK: - Properties

    private var innerDataSource: [SelectableOption] = []
    var dataSource: [SelectableOption] {
        get {
            return self.innerDataSource
        }
        set {
            self.innerDataSource = newValue

            self.selectedIndexPath = nil
            for var i: Int = 0; i < newValue.count; i++ {
                if newValue[i].isSelected {
                    self.selectedIndexPath = NSIndexPath(forRow: i, inSection: 0)
                    break
                }
            }
        }
    }
    var onSelectedItem: OnSelectedItemF
    var onSelectionCancelled: OnSelectionCancelledF
    var selectedIndexPath: NSIndexPath? {
        willSet {
            if let n = newValue {
                if n != self.selectedIndexPath && !self.innerDataSource[n.row].isSelected {
                    self.innerDataSource[n.row].isSelected = true
                    if let o = self.selectedIndexPath {
                        self.innerDataSource[o.row].isSelected = false
                    }
                }
            }
        }
    }

    //
    // MARK: - Overrides
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.clearsSelectionOnViewWillAppear = true
    }
    
    //
    // MARK: - Support Methods
    
    
    //
    // MARK: - Action Methods

    @IBAction func done(sender: UIBarButtonItem) {
        if let i = self.selectedIndexPath {
            var item = self.dataSource[i.row]
            onSelectedItem?(item)
        } else {
            var alert = UIAlertView(title: "Traqt",
                message: "É necessário escolher um item.",
                delegate: nil,
                cancelButtonTitle: "OK")
            alert.show()
        }
    }
    
    @IBAction func cancelled(sender: UIBarButtonItem) {
        onSelectionCancelled?()
    }
    
    //
    // MARK: - UITableViewDataSource
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSource.count
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var item = self.dataSource[indexPath.row]

        var cell = self.tableView.dequeueReusableCellWithIdentifier(item.cellIdentifier) as? UITableViewCell
        if cell == nil {
            cell = UITableViewCell(style: item.cellStyle, reuseIdentifier: item.cellIdentifier)
        }
        
        cell!.textLabel?.text = item.title
        if let st = item.subTitle {
            cell!.detailTextLabel!.text = st
        }
        cell!.accessoryType = item.isSelected ? .Checkmark : .None
        
        return cell!
    }
    
    //
    // MARK: - UITableViewDelegate
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var indexPaths = [ indexPath ]
        if let o = self.selectedIndexPath {
            indexPaths.append(o)
        }
        
        self.selectedIndexPath = indexPath
        self.tableView.reloadRowsAtIndexPaths(indexPaths, withRowAnimation: UITableViewRowAnimation.Automatic)
    }
    
}