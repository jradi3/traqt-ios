//
//  RemindTableViewController.swift
//  Traqt
//
//  Created by Tarek Abdala Rfaei Jradi on 9/26/14.
//  Copyright (c) 2014 Syligo. All rights reserved.
//

import Foundation

protocol WeekDaysPickerControllerDelegate {
    func weekDaysPicker(controller: WeekDaysPickerController, didPickWeekDays weekdays: WeekDays)
    func weekDaysPickerDidCancel(controller: WeekDaysPickerController)
}

class WeekDaysPickerController: UITableViewController {

    //
    // MARK: - Properties
    
    var delegate: WeekDaysPickerControllerDelegate?
    var selectedWeekDays: WeekDays = .None
    var weekDays: [WeekDays.WeekDayInfo]!
    
    //
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        weekDays = selectedWeekDays.asWeekDays()
    }
    
    //
    // MARK: - Action methods
    
    @IBAction func done(sender: UIBarButtonItem) {
        self.delegate?.weekDaysPicker(self, didPickWeekDays: self.selectedWeekDays)
        
    }
    
    @IBAction func cancelled(sender: UIBarButtonItem) {
        self.delegate?.weekDaysPickerDidCancel(self)
    }
    
    //
    // MARK: - UITableViewDataSource
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weekDays.count
    }
    
    //
    // MARK: - Table View Delegate

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "Cell")
        let weekDay = weekDays[indexPath.row]
        
        cell.accessoryType = weekDay.selected ? .Checkmark : .None
        cell.textLabel?.text = weekDay.longName
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let selectedCell = tableView.cellForRowAtIndexPath(indexPath)
        if selectedCell?.accessoryType == UITableViewCellAccessoryType.Checkmark {
            selectedCell?.accessoryType = .None
            self.weekDays[indexPath.row].selected = false
            self.selectedWeekDays ^= weekDays[indexPath.row].weekDaysValue!
        } else {
            selectedCell?.accessoryType = .Checkmark
            self.weekDays[indexPath.row].selected = true
            self.selectedWeekDays |= weekDays[indexPath.row].weekDaysValue!
        }
        selectedCell?.setSelected(false, animated: true)
    }

}