//
//  CircularSliderInputView.swift
//  Traqt
//
//  Created by Tarek Abdala Rfaei Jradi on 9/19/14.
//  Copyright (c) 2014 Syligo. All rights reserved.
//

import Foundation

protocol CircularSliderInputViewDelegate {
    func slideHoursDidChange(slider : EFCircularSlider)
    func slideMinutesDidChange(slider : EFCircularSlider)
}

class CircularSliderInputView: UIViewController {

    //
    // MARK: - Constants
    
    let sliderMinuteWidth = CGFloat(320)
    let sliderMinuteHeight = CGFloat(220)
    
    //
    // MARK: - Properties
    
    var sliderHours: EFCircularSlider!
    var sliderMinutes: EFCircularSlider!
    var delegate: CircularSliderInputViewDelegate?

    //
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UINavigationBar.appearance().barTintColor
        
        let sliderMinuteFrame = CGRect(
            x: 0,
            y: 0,
            width: sliderMinuteWidth,
            height: sliderMinuteHeight)
        self.sliderHours = EFCircularSlider(frame: sliderMinuteFrame)
        self.sliderHours.unfilledColor = UIColor(red: 23/255.0, green: 47/255.0, blue: 70/255.0, alpha: 1.0)
        self.sliderHours.filledColor = UIColor(red: 98/255.0, green: 243/255.0, blue: 252/255.0, alpha: 1.0)
        self.sliderHours.minimumValue = 0
        self.sliderHours.maximumValue = 60
        self.sliderHours.lineWidth = 8
        self.sliderHours.snapToLabels = false
        self.sliderHours.labelFont = UIFont.systemFontOfSize(12.0)
        self.sliderHours.labelColor = UIColor(red: 127/255.0, green: 229/255.0, blue: 255/255.0, alpha: 1.0)
        self.sliderHours.handleType = bigCircle
        self.sliderHours.handleColor = self.sliderHours.filledColor
        let hoursArray = ["5", "10", "15", "20", "25", "30", "35", "40", "45", "50", "55", "60"]
        self.sliderHours.setInnerMarkingLabels(hoursArray)
        self.sliderHours.addTarget(self, action:"hoursDidChange:", forControlEvents:UIControlEvents.ValueChanged)
        self.view.addSubview(self.sliderHours)

        let sliderSecondFrame = CGRectMake(0, 0, 150, 145)
        self.sliderMinutes = EFCircularSlider(frame: sliderSecondFrame)
        self.sliderMinutes.unfilledColor = UIColor(red: 23/255.0, green: 47/255.0, blue: 70/255.0, alpha: 1.0)
        self.sliderMinutes.filledColor = UIColor(red: 155/255.0, green: 211/255.0, blue: 156/255.0, alpha: 1.0)
        self.sliderMinutes.minimumValue = 0
        self.sliderMinutes.maximumValue = 60
        self.sliderMinutes.lineWidth = 8
        self.sliderMinutes.labelFont = UIFont.systemFontOfSize(14.0)
        self.sliderMinutes.labelColor = UIColor(red: 76/255.0, green: 111/255.0, blue: 137/255.0, alpha: 1.0)
        self.sliderMinutes.handleType = doubleCircleWithOpenCenter
        self.sliderMinutes.handleColor = self.sliderMinutes.filledColor
        let minutesArray = ["5", "10", "15", "20", "25", "30", "35", "40", "45", "50", "55", "60"]
        self.sliderMinutes.setInnerMarkingLabels(minutesArray)
        self.sliderMinutes.addTarget(self, action:"minutesDidChange:", forControlEvents: UIControlEvents.ValueChanged)
        self.view.addSubview(self.sliderMinutes)
        
        self.sliderMinutes.center = self.sliderHours.center
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        // Re-calculates the frames of the views
        self.sliderHours.frame = CGRect(x: (self.view.frame.width / 2) - (self.sliderHours.frame.width / 2),
            y: (self.view.frame.height / 2) - (self.sliderHours.frame.height / 2),
            width: self.sliderHours.frame.width,
            height: self.sliderHours.frame.height)
        
        self.sliderMinutes.center = self.sliderHours.center
    }

    func hoursDidChange(slider : EFCircularSlider) {
        self.delegate?.slideHoursDidChange(slider)
    }
    
    func minutesDidChange(slider : EFCircularSlider) {
        self.delegate?.slideMinutesDidChange(slider)
    }
}