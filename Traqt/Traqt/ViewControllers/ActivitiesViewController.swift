//
//  ActivitiesViewController.swift
//  Traqt
//
//  Created by Rafael Veronezi on 8/15/14.
//  Copyright (c) 2014 Syligo. All rights reserved.
//

import UIKit
import CoreData
import CoreDataContext

class ActivitiesViewController: CoreDataTableViewController, UISearchBarDelegate, UISearchDisplayDelegate {
    
    var dataContext = TraqtDataContext.sharedInstance
    
    //Array to Search Result at UISearchBar
    var filteredActivities = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
         self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        self.navigationItem.leftBarButtonItem = self.editButtonItem()
        self.fetchedResultsController = dataContext.activities.getAllAsFetchedResultsController(sortDescriptors: NSSortDescriptor(key: "listOrder", ascending: true))
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.searchDisplayController!.searchResultsTableView {
            return self.filteredActivities.count
        } else {
            return dataContext.activities.getAll().count
        }
    }

    //
    // MARK: - Table view data source

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCellWithIdentifier("ActivityCell") as UITableViewCell
        
        var activity = self.fetchedResultsController!.objectAtIndexPath(indexPath) as Activity
        
        // Check search results table is being displayed and set the activity object from the appropriate array
        if tableView == self.searchDisplayController!.searchResultsTableView {
            activity = filteredActivities[indexPath.row] as Activity
        }
        
        //Configure the cell
        cell.textLabel?.text = activity.name
        return cell
    }

    // MARK: - UITableViewDelegate

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let indexPathT = (indexPath.section, indexPath.row)
        var activity = self.fetchedResultsController!.objectAtIndexPath(indexPath) as Activity
        
        // If Search Results
        if tableView == self.searchDisplayController!.searchResultsTableView {
            activity = filteredActivities[indexPath.row] as Activity
        }
        
        self.performSegueWithIdentifier("ActivityForm", sender: activity);
    }
    
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            let activity = self.fetchedResultsController!.objectAtIndexPath(indexPath) as Activity
            self.dataContext.activities.delete(activity.activityId)
        }
    }

    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
        var activities = self.fetchedResultsController!.fetchedObjects as [Activity]
        self.fetchedResultsController = nil

        let fromObj = activities[fromIndexPath.row] as Activity
        let toObj = activities[toIndexPath.row] as Activity
        activities.removeAtIndex(fromIndexPath.row)
        activities.insert(fromObj, atIndex: toIndexPath.row)

        var i = 0
        for activity in activities {
            activity.listOrder = i++
        }
        
        dataContext.saveContext()
        self.fetchedResultsController = dataContext.activities.getAllAsFetchedResultsController(sortDescriptors: NSSortDescriptor(key: "listOrder", ascending: true))
    }

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView!, canMoveRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */
    
    //
    // MARK: - Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        // Verifica se o View Controller de destino é um Navigation Controller
        if let nvc = segue.destinationViewController as? UINavigationController {
            // Extrai o primeiro View Controller na navegação
            if let vc = nvc.topViewController as? ActivityFormViewController {
                if let a = sender as? Activity {
                    vc.activity = a
                }
            }
        }
    }
    
    //
    // MARK: - Filtered Context
    
    func filterContentForSearchText(searchText: String) {
        let predicate = NSPredicate(format: "name CONTAINS[cd] %@", searchText)!
        self.filteredActivities = self.dataContext.activities.filter(predicate, sortDescriptors: [ NSSortDescriptor(key: "name", ascending: true) ])!
    }
    
    // MARK: - UISearchBarDelegate

    //These methods are part of the UISearchDisplayControllerDelegate protocol.
    func searchDisplayController(controller: UISearchDisplayController!, shouldReloadTableForSearchString searchString: String!) -> Bool {
        self.filterContentForSearchText(searchString)
        return true
    }
    func searchDisplayController(controller: UISearchDisplayController!, shouldReloadTableForSearchScope searchOption: Int) -> Bool {
        self.filterContentForSearchText(self.searchDisplayController!.searchBar.text)
        return true
    }
    
}
