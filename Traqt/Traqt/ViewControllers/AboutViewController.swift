//
//  AboutViewController.swift
//  Traqt
//
//  Created by Tarek Abdala Rfaei Jradi on 9/18/14.
//  Copyright (c) 2014 Syligo. All rights reserved.
//

import Foundation
import WebKit


class AboutViewController : UIViewController {

    //
    // MARK: - Outlets
    
    @IBOutlet weak var versionLabel: UILabel!
    
    //
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad();
        
        let version = NSBundle.mainBundle().objectForInfoDictionaryKey("CFBundleShortVersionString") as? String
        let build = NSBundle.mainBundle().objectForInfoDictionaryKey(kCFBundleVersionKey) as? String
        self.versionLabel.text = "Versão \(version!) (Build \(build!))"
    }
}