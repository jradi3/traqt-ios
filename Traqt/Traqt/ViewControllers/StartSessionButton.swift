//
//  StartSessionButton.swift
//  Traqt
//
//  Created by Tarek Abdala Rfaei Jradi on 10/3/14.
//  Copyright (c) 2014 Syligo. All rights reserved.
//

import Foundation

class StartSessionButton : UIButton {
 
    let progressLine = CAShapeLayer()
    let basicAnimate = CABasicAnimation(keyPath: "strokeEnd")
    let ovalPath = UIBezierPath()

    override init() {
        super.init()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        // set up some values to use in the curve
        let ovalStartAngle = CGFloat(90.01 * M_PI/90)
        let ovalEndAngle = CGFloat(90 * M_PI/90)
        let ovalRect = CGRectMake(
            self.frame.size.width/2,
            self.frame.size.height/2,
            self.frame.size.width-30,
            self.frame.size.height-30)
        
        // Set values to the bezier path
        ovalPath.addArcWithCenter(CGPointMake(self.frame.size.width/2, self.frame.size.height/2),
            radius: CGRectGetWidth(ovalRect) / 2,
            startAngle: ovalStartAngle,
            endAngle: ovalEndAngle, clockwise: true)
        
        // create an object that represents how the curve
        // should be presented on the screen
        progressLine.path = ovalPath.CGPath
        progressLine.strokeColor = UIColor(red: 1/255, green: 84/255, blue:139/255, alpha: 1.0).CGColor
        progressLine.fillColor = UIColor.clearColor().CGColor
        progressLine.lineWidth = 17.0
        progressLine.lineCap = kCAFillRuleNonZero
        
        // Set a basic animation that animates the value 'strokeEnd'
        // from 0.0 to 1.0 over 3.0 seconds
        basicAnimate.duration = 0.5
        basicAnimate.fromValue = 0.0
        basicAnimate.toValue = 1.0
        basicAnimate.fillMode = kCAFillModeForwards
        basicAnimate.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
    }
    
    //
    // MARK - Class Functions
    func startAnimate() {
        self.layer.addSublayer(progressLine)
        progressLine.addAnimation(basicAnimate, forKey: "animate stroke end animation")
    }
    func stopAnimate(){
        self.progressLine.removeAllAnimations()
        self.progressLine.removeFromSuperlayer()
    }
    func clearAllLayers(){
        self.progressLine.removeAllAnimations()
        self.progressLine.removeFromSuperlayer()
    }
    
    //
    // MARK - UIResponder Delegate
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        self.startAnimate()
    }
    
    override func touchesEnded(touches: NSSet, withEvent event: UIEvent) {
        self.stopAnimate()
    }
}