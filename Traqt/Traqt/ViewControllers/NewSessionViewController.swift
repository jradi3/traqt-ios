//
//  NewSessionViewController.swift
//  Traqt
//
//  Created by Rafael Veronezi on 8/15/14.
//  Copyright (c) 2014 Syligo. All rights reserved.
//

import UIKit

class NewSessionViewController: UIViewController {
    
    //
    // MARK: - Outlets
    
    @IBOutlet weak var activityButton: StartSessionButton!
    @IBOutlet weak var selectActivityButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Set Delegate Animate
        activityButton.basicAnimate.delegate = self
    }
    
    //
    // MARK: - Properties
    
    var selectedActivity: Activity? {
        didSet {
            if let a = selectedActivity {
                selectActivityButton.setTitle(a.name, forState: UIControlState.Normal)
            } else {
                selectActivityButton.setTitle("Escolha sua Atividade", forState: UIControlState.Normal)
            }
        }
    }
    
    //
    // MARK: - ActivityButton Animation Delegate
    
    override func animationDidStart(anim: CAAnimation!) {
        if self.selectedActivity == nil {
            selectActivityButton.setTitle("Oi! Antes escolha sua atividade", forState: UIControlState.Normal)
            activityButton.stopAnimate()
        }
    }
    override func animationDidStop(anim: CAAnimation!, finished flag: Bool) {
        // If User have selected some Activity and
        // flag from basicAnimate finished is true. Then
        // Push Segue StartSession
        if self.selectedActivity != nil && flag {
            self.activityButton.clearAllLayers()
            self.performSegueWithIdentifier("StartSession", sender: nil)
        }
    }
    
    //
    // MARK: - Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if let vc = segue.destinationViewController as? SessionViewController {
            if let a = self.selectedActivity {
                vc.activityId = a.activityId
            }
        }
    }
    
    //
    // MARK: - Action Methods
    
    @IBAction func newSession(sender: UIButton) {
        var optionsController = OptionsController.optionsControllerWithItems(TraqtDataContext.sharedInstance.activities.getAllAsOption("name", sortKey: "listOrder") {
                (activity: Activity) in
                return self.selectedActivity?.activityId == activity.activityId
            })
        optionsController.title = "Escolha a Atividade"
        optionsController.onSelectedItem = {
            (item: SelectableOption) in
            self.selectedActivity = (item.sourceObject as Activity)
            self.dismissViewControllerAnimated(true, completion: nil)
        }
        optionsController.onSelectionCancelled = {
            self.dismissViewControllerAnimated(true, completion: nil)
        }
        self.presentViewController(optionsController, animated: true, completion: nil)
    }
    
}
