//
//  Weekdays.swift
//  Traqt
//
//  Created by Rafael Veronezi on 9/30/14.
//  Copyright (c) 2014 Syligo. All rights reserved.
//

import Foundation

/**
    Allow selection of multiple WeekDays
 */
public struct WeekDays : RawOptionSetType, BooleanType {
    
    /**
        Information about some WeekDay
     */
    public struct WeekDayInfo {
        
        var longName: String
        var shortName: String
        var value: Int
        var weekDaysValue: WeekDays? {
            return WeekDays(rawValue: value)
        }
        var selected: Bool = false
        
        init(weekDay: WeekDay) {
            self.longName = WeekDayInfo.getLongWeekDayName(weekDay)
            self.shortName = WeekDayInfo.getShortWeekDayName(weekDay)
            switch weekDay {
            case .Sunday:
                self.value = WeekDays.Sunday.rawValue
            case .Monday:
                self.value = WeekDays.Monday.rawValue
            case .Tuesday:
                self.value = WeekDays.Tuesday.rawValue
            case .Wednesday:
                self.value = WeekDays.Wednesday.rawValue
            case .Thursday:
                self.value = WeekDays.Thursday.rawValue
            case .Friday:
                self.value = WeekDays.Friday.rawValue
            case .Saturday:
                self.value = WeekDays.Saturday.rawValue
            }
        }
        
        init(weekDay: WeekDay, isSelected: Bool) {
            self.init(weekDay: weekDay)
            self.selected = isSelected
        }
        
        //
        // MARK: - Static Helpers to Find Localized Week Day Names
        
        static let gregorianCalendar = NSCalendar(calendarIdentifier: NSGregorianCalendar)!
        static let longWeekDayNameDateFormatter: NSDateFormatter = {
            var formatter = NSDateFormatter()
            formatter.dateFormat = "EEEE"
            return formatter
        }()
        static func getLongWeekDayName(weekDay: WeekDay) -> String {
            var dateComponents = NSDateComponents()
            dateComponents.weekday = weekDay.rawValue
            dateComponents.weekdayOrdinal = 1
            dateComponents.year = 2014
            dateComponents.month = 1
            
            var tempDate = gregorianCalendar.dateFromComponents(dateComponents)
            return longWeekDayNameDateFormatter.stringFromDate(tempDate!)
        }
        
        static let shortWeekDayNameDateFormatter: NSDateFormatter = {
            var formatter = NSDateFormatter()
            formatter.dateFormat = "E"
            return formatter
        }()
        static func getShortWeekDayName(weekDay: WeekDay) -> String {
            var dateComponents = NSDateComponents()
            dateComponents.weekday = weekDay.rawValue
            dateComponents.weekdayOrdinal = 1
            dateComponents.year = 2014
            dateComponents.month = 1
            
            var tempDate = gregorianCalendar.dateFromComponents(dateComponents)
            return shortWeekDayNameDateFormatter.stringFromDate(tempDate!)
        }
        
    }
    
    //
    // MARK: - Properties
    
    private var value: Int = 0
    public var boolValue: Bool { return value != 0 }
    public var rawValue: Int {
        return value
    }
    
    static public let weekDayInfos: [WeekDayInfo] = {
        var weekDays = [WeekDayInfo]()
        weekDays.append(WeekDayInfo(weekDay: .Sunday))
        weekDays.append(WeekDayInfo(weekDay: .Monday))
        weekDays.append(WeekDayInfo(weekDay: .Tuesday))
        weekDays.append(WeekDayInfo(weekDay: .Wednesday))
        weekDays.append(WeekDayInfo(weekDay: .Thursday))
        weekDays.append(WeekDayInfo(weekDay: .Friday))
        weekDays.append(WeekDayInfo(weekDay: .Saturday))
        
        return weekDays
    }()
    
    //
    // MARK: - Initializers

    public init(rawValue value: Int) {
        self.value = value
    }
    
    public init(nilLiteral: ()) {
        self.value = 0
    }
    
    //
    // MARK: - Methods
    
    static public func fromMask(raw: Int) -> WeekDays { return self(rawValue: raw) }
    static public var allZeros: WeekDays { return self(rawValue: 0) }

    public func toString() -> String {
        if self == .None {
            return "Nenhum"
        }
        
        var days: String = ""
        if self == (WeekDays.Sunday | WeekDays.Monday | WeekDays.Tuesday | WeekDays.Wednesday | WeekDays.Thursday | WeekDays.Friday | WeekDays.Saturday) {
            days = "Todos os Dias"
        } else if self == (WeekDays.Sunday | WeekDays.Saturday) {
            days = "Fim de Semana"
        } else if self == (WeekDays.Monday | WeekDays.Tuesday | WeekDays.Wednesday | WeekDays.Thursday | WeekDays.Friday) {
            days = "Dias da Semana"
        } else {
            var isFirst = true
            if self == (self | WeekDays.Sunday) {
                days += WeekDays.weekDayInfos[0].shortName
                isFirst = false
            }
            if self == (self | WeekDays.Monday) {
                days += (isFirst ? "" : " ") + WeekDays.weekDayInfos[1].shortName
                isFirst = false
            }
            if self == (self | WeekDays.Tuesday) {
                days += (isFirst ? "" : " ") + WeekDays.weekDayInfos[2].shortName
                isFirst = false
            }
            if self == (self | WeekDays.Wednesday) {
                days += (isFirst ? "" : " ") + WeekDays.weekDayInfos[3].shortName
                isFirst = false
            }
            if self == (self | WeekDays.Thursday) {
                days += (isFirst ? "" : " ") + WeekDays.weekDayInfos[4].shortName
                isFirst = false
            }
            if self == (self | WeekDays.Friday) {
                days += (isFirst ? "" : " ") + WeekDays.weekDayInfos[5].shortName
                isFirst = false
            }
            if self == (self | WeekDays.Saturday) {
                days += (isFirst ? "" : " ") + WeekDays.weekDayInfos[6].shortName
            }
        }
        
        return days
        
    }
    
    public func asWeekDays() -> [WeekDayInfo] {
        var weekDays = [WeekDayInfo]()
        var isSelected: Bool
        
        isSelected = (self & WeekDays.Sunday) ? true : false
        weekDays.append(WeekDayInfo(weekDay: .Sunday, isSelected: isSelected))
        isSelected = (self & WeekDays.Monday) ? true : false
        weekDays.append(WeekDayInfo(weekDay: .Monday, isSelected: isSelected))
        isSelected = (self & WeekDays.Tuesday) ? true : false
        weekDays.append(WeekDayInfo(weekDay: .Tuesday, isSelected: isSelected))
        isSelected = (self & WeekDays.Wednesday) ? true : false
        weekDays.append(WeekDayInfo(weekDay: .Wednesday, isSelected: isSelected))
        isSelected = (self & WeekDays.Thursday) ? true : false
        weekDays.append(WeekDayInfo(weekDay: .Thursday, isSelected: isSelected))
        isSelected = (self & WeekDays.Friday) ? true : false
        weekDays.append(WeekDayInfo(weekDay: .Friday, isSelected: isSelected))
        isSelected = (self & WeekDays.Saturday) ? true : false
        weekDays.append(WeekDayInfo(weekDay: .Saturday, isSelected: isSelected))
        
        return weekDays
    }
    
    //
    // MARK: - Options
    
    static public var None: WeekDays       { return self(rawValue: 0b00000000) }
    static public var Sunday: WeekDays     { return self(rawValue: 0b00000001) }
    static public var Monday: WeekDays     { return self(rawValue: 0b00000010) }
    static public var Tuesday: WeekDays    { return self(rawValue: 0b00000100) }
    static public var Wednesday: WeekDays  { return self(rawValue: 0b00001000) }
    static public var Thursday: WeekDays   { return self(rawValue: 0b00010000) }
    static public var Friday: WeekDays     { return self(rawValue: 0b00100000) }
    static public var Saturday: WeekDays   { return self(rawValue: 0b01000000) }
    
    
}