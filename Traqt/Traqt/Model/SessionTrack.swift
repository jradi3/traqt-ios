//
//  SessionTrack.swift
//  Traqt
//
//  Created by Rafael Veronezi on 8/25/14.
//  Copyright (c) 2014 Syligo. All rights reserved.
//

import Foundation
import ChronometerKit

// SessionTrack
// Uma instância da classe SessionTrack seria a responsável por manter o estado
//  de uma sessão em andamento. A partir de uma atividade, ela determina o que será
//  monitorado na sessão (tempo e/ou repetições), e a partir disso mantém controla
//  o estado, sendo o modelo do ViewController da sessão
//
// Funções dessa classe:
//  - Mantém o registro de repetições:
//      - Método para incrementar o contador
//      - Método para acessar o contador atual
//      - Método para acessar o limite de repetições
//  - Mantém o registro de tempo:
//      - Mantém um cronometro interno para registra o tempo transcorrido/restante
//      - Repassa os blocos do contador interno para que a interface se atualize
// - Mantém o registro de andamento
//      - Informa a interface quando a sessão é finalizada conforme o critério

public class SessionTrack {
    
    public enum SessionState {
        case New
        case Running
        case Paused
        case Completed
        case Cancelled
    }
    
    //
    // MARK: - Properties
    
    public private(set) var sessionState: SessionState = .New
    public private(set) var activity: Activity
    public private(set) var currentRepetitions: Int = 0
    public var enableVibration: Bool
    public var repetitionsProgress: Float {
        return Float(currentRepetitions) / activity.repetitions.floatValue
    }
    public private(set) var startTime: NSDate?
    private var chronometer = Chronometer()
    public var updateTimerBlock: ChronometerUpdateBlock {
        didSet {
            self.chronometer.updateBlock = updateTimerBlock
        }
    }
    public var completionBlock: (() -> ())?
    
    //
    // MARK: - Initializers
    
    init(activity: Activity) {
        self.activity = activity
        self.enableVibration = activity.enableVibration.boolValue
        
        // Configure the chronometer
        if activity.measureTime.boolValue && activity.timeLimit.integerValue > 0 {
            self.chronometer.setLimit(NSTimeInterval(self.activity.timeLimit.floatValue)) {
                self.completeSession()
            }
        }
    }
    
    convenience init(activityId: String) {
        var activity = TraqtDataContext.sharedInstance.activities.find(activityId)
        self.init(activity: activity!)
    }
    
    //
    // MARK: - Public Methods
    
    public func addRepetition() {
        if sessionState == .Running {
            self.currentRepetitions++
            self.checkCompletion()
        }
    }
    
    public func startSession() {
        // Só deve fazer alguma coisa se a sessão for nova
        if sessionState == .New {
            self.startTime = NSDate()       // Marca o horário de início
            self.chronometer.start()
            self.sessionState = .Running
        }
    }
    
    public func pauseSession() {
        if sessionState == .Running {
            self.chronometer.stop()
            self.sessionState = .Paused
        }
    }
    
    public func resumeSession() {
        if sessionState == .Paused {
            self.chronometer.start()
            self.sessionState = .Running
        }
    }
    
    public func cancelSession() {
        self.completeSession(isCancelled: true)
    }
    
    //
    // MARK: - Private Methods

    private func completeSession(isCancelled: Bool = false) {
        if sessionState != .Cancelled && sessionState != .Completed {
            self.chronometer.stop()
            self.sessionState = isCancelled ? .Cancelled : .Completed
            let outcome: SessionOutcome = isCancelled ? .Cancelled : .Completed
            self.activity.addSession(outcome, startTime: startTime!, elapsedTime: self.chronometer.elapsedTime, repetitions: self.currentRepetitions)
            self.completionBlock?()
        }
    }
    
    /// Esse método avalia os critérios da atividade selecionada com o estado atual da sessão
    /// para determinar se a sessão foi concluída.
    private func checkCompletion() {
        // Verifica se esta medindo as repetições e se o limite foi atingido
        if self.activity.measureRepetitions.boolValue {
            if self.currentRepetitions >= self.activity.repetitions.integerValue {
                self.completeSession()
                completionBlock?()
                return
            }
        }
    }
    
}