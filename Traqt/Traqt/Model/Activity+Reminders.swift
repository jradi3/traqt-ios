//
//  Activity+Notifications.swift
//  Traqt
//
//  Created by Rafael Veronezi on 10/1/14.
//  Copyright (c) 2014 Syligo. All rights reserved.
//

import Foundation

extension Activity {
    
    //
    // MARK: - Overrides
    
    override public func didSave() {
        super.didSave()
        
        // Clear previous related records
        clearNotificationsOfActivity(self.activityId)
        updateSharedStorage()
        
        // Operations to do if it's not deletion of this record
        if !deleted {
            scheduleLocalNotifications()
        }
    }
    
    //
    // MARK: - Helper Methods
    
    
    func updateSharedStorage() {
        let sharedUserDefaults = NSUserDefaults(suiteName: "group.com.syligo.traqt.extensions")!
        var activities: [NSDictionary]
        
        if var sharedActivities = sharedUserDefaults.objectForKey("SharedActivities") as? [NSDictionary] {
            // First search for the activity in the list to remove
            var activityIndex = -1
            for var i = 0; i < sharedActivities.count; i++ {
                let activity = SharedActivity(info: sharedActivities[i])
                if activity.activityId == self.activityId {
                    activityIndex = i
                    break
                }
            }
            if activityIndex >= 0 {
                sharedActivities.removeAtIndex(activityIndex)
            }
            
            activities = sharedActivities
        } else {
            // If no dictionary exists before create a new one
            activities = [NSDictionary]()
        }

        // Add the activity to the shared activity list
        if !deleted && reminders.boolValue {
            activities.append(self.toSharedActivityDictionary())
        }
        
        sharedUserDefaults.setObject(activities, forKey: "SharedActivities")
        sharedUserDefaults.synchronize()
    }
    
    func scheduleLocalNotifications() {
        scheduleLocalNotifications(forActivity: self)
    }
    
    func scheduleLocalNotifications(forActivity activity: Activity) {
        // Check if reminders are enabled
        if !activity.reminders.boolValue {
            return
        }
        
        // Adiciona recorrencia de notificações locais no horário especificado
        let reminderTime = activity.reminderTime.integerValue
        if reminderTime >= Int.TIME_MIN && reminderTime <= Int.TIME_MAX {
            let alertBody = "Não se esqueça de sua atividade \"\(activity.name)\"!"
            if activity.reminderWeekDays & .Sunday {
                scheduleWeeklyLocalNotification(reminderTime, weekDay: .Sunday, withBody: alertBody, identifiedBy: activity.activityId)
            }
            if activity.reminderWeekDays & .Monday {
                scheduleWeeklyLocalNotification(reminderTime, weekDay: .Monday, withBody: alertBody, identifiedBy: activity.activityId)
            }
            if activity.reminderWeekDays & .Tuesday {
                scheduleWeeklyLocalNotification(reminderTime, weekDay: .Tuesday, withBody: alertBody, identifiedBy: activity.activityId)
            }
            if activity.reminderWeekDays & .Wednesday {
                scheduleWeeklyLocalNotification(reminderTime, weekDay: .Wednesday, withBody: alertBody, identifiedBy: activity.activityId)
            }
            if activity.reminderWeekDays & .Thursday {
                scheduleWeeklyLocalNotification(reminderTime, weekDay: .Thursday, withBody: alertBody, identifiedBy: activity.activityId)
            }
            if activity.reminderWeekDays & .Friday {
                scheduleWeeklyLocalNotification(reminderTime, weekDay: .Friday, withBody: alertBody, identifiedBy: activity.activityId)
            }
            if activity.reminderWeekDays & .Saturday {
                scheduleWeeklyLocalNotification(reminderTime, weekDay: .Saturday, withBody: alertBody, identifiedBy: activity.activityId)
            }
        }
    }
    
    func scheduleWeeklyLocalNotification(reminderTime: Int, weekDay: WeekDay, withBody body: String, identifiedBy key: String) {
        if let baseDate = NSDate().getNextDate(forWeekDay: weekDay) {
            let fireDate = reminderTime.timeIntToDate(baseDate)!
            let notification = UILocalNotification()
            notification.fireDate = fireDate
            notification.alertBody = body
            notification.soundName = UILocalNotificationDefaultSoundName
            notification.repeatInterval = .WeekCalendarUnit
            notification.applicationIconBadgeNumber = 1
            notification.userInfo = [ "NotificationKey": key ]
            
            UIApplication.sharedApplication().scheduleLocalNotification(notification)
        }
    }
    
    func clearNotificationsOfActivity(id: String) {
        var notificationsToCancel = [UILocalNotification]()
        for notification in UIApplication.sharedApplication().scheduledLocalNotifications as [UILocalNotification] {
            if let userInfo = notification.userInfo {
                if let notificationId = userInfo["NotificationKey"] as? NSString {
                    if notificationId == id {
                        notificationsToCancel.append(notification)
                    }
                }
            }
        }
        
        for notification in notificationsToCancel {
            UIApplication.sharedApplication().cancelLocalNotification(notification)
        }
    }
    
    func toSharedActivityDictionary() -> NSDictionary {
        var activityData = NSMutableDictionary()
        activityData[kSharedActivityFieldId] = self.activityId
        activityData[kSharedActivityFieldName] = self.name
        activityData[kSharedActivityFieldDaysOfWeek] = self.reminderDays
        activityData[kSharedActivityFieldTimeOfDay] = self.reminderTime
        return activityData
    }
    
}