//
//  SessionOutcome.swift
//  Traqt
//
//  Created by Rafael Veronezi on 9/18/14.
//  Copyright (c) 2014 Syligo. All rights reserved.
//

import Foundation

enum SessionOutcome : Int {
    case Completed
    case Cancelled
}
