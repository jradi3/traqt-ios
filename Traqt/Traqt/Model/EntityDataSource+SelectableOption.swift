//
//  EntityDataSource+SelectableOption.swift
//  Traqt
//
//  Created by Rafael Veronezi on 9/19/14.
//  Copyright (c) 2014 Syligo. All rights reserved.
//

import Foundation
import CoreData
import CoreDataContext

extension EntityDataSource {

    func getAllAsOption(titleFieldName: String, sortKey: String?, isSelected: (T) -> (Bool)) -> [SelectableOption] {
        var options = SelectableOption.generateOptionList(self.getAll(NSSortDescriptor(key: sortKey ?? titleFieldName, ascending: true))) {
            var option = SelectableOption($0.valueForKey(titleFieldName) as String)
            option.sourceObject = $0
            option.isSelected = isSelected($0)
            return option
        }
        return options
    }
    
}