//
//  Session.swift
//  Traqt
//
//  Created by Rafael Veronezi on 9/18/14.
//  Copyright (c) 2014 Syligo. All rights reserved.
//

import Foundation
import CoreData

class Session: NSManagedObject {

    @NSManaged var sessionId: String
    @NSManaged var sessionOutcome: NSNumber
    @NSManaged var startTime: NSDate
    @NSManaged var endTime: NSDate
    @NSManaged var totalRepetitions: NSNumber
    @NSManaged var elapsedTime: NSNumber
    @NSManaged var activity: Traqt.Activity

}
