//
//  Traqt.swift
//  Traqt
//
//  Created by Rafael Veronezi on 9/18/14.
//  Copyright (c) 2014 Syligo. All rights reserved.
//

import Foundation
import CoreData

public class Activity: NSManagedObject {

    @NSManaged var activityId: String
    @NSManaged var details: String?
    @NSManaged var enableCountdown: NSNumber
    @NSManaged var enableVibration: NSNumber
    @NSManaged var measureRepetitions: NSNumber
    @NSManaged var measureTime: NSNumber
    @NSManaged var name: String
    @NSManaged var listOrder: NSNumber
    @NSManaged var repetitions: NSNumber
    @NSManaged var theme: NSNumber
    @NSManaged var timeLimit: NSNumber
    @NSManaged var reminders: NSNumber
    @NSManaged var reminderDays: NSNumber
    @NSManaged var reminderTime: NSNumber
    @NSManaged var sessions: NSSet

}
