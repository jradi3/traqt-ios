//
//  WeekDay.swift
//  Traqt
//
//  Created by Rafael Veronezi on 11/15/14.
//  Copyright (c) 2014 Syligo. All rights reserved.
//

import Foundation

/**
    Enumera os dias da semana para utilização com o Helper.
 */
enum WeekDay : Int {
    case Sunday = 1
    case Monday
    case Tuesday
    case Wednesday
    case Thursday
    case Friday
    case Saturday
    
    func asWeekDays() -> WeekDays {
        switch self {
        case .Sunday:
            return WeekDays.Sunday
        case .Monday:
            return WeekDays.Monday
        case .Tuesday:
            return WeekDays.Tuesday
        case .Wednesday:
            return WeekDays.Wednesday
        case .Thursday:
            return WeekDays.Thursday
        case .Friday:
            return WeekDays.Friday
        case .Saturday:
            return WeekDays.Saturday
        default:
            return WeekDays.None
        }
    }
}