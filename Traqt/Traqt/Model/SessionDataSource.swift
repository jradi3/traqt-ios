//
//  SessionDataSource.swift
//  Traqt
//
//  Created by Rafael Veronezi on 10/1/14.
//  Copyright (c) 2014 Syligo. All rights reserved.
//

import Foundation
import CoreData
import CoreDataContext

typealias SessionDataSource = SessionDataSourceGeneric<AnyObject>

class SessionDataSourceGeneric<T> : EntityDataSource<Session> {
    
    //
    // MARK: - Initialization
    
    init(managedObjectContext: NSManagedObjectContext) {
        super.init(managedObjectContext: managedObjectContext, entityName: TraqtDataContext.ENTITY_SESSION_NAME, entityKeyName: TraqtDataContext.ENTITY_SESSION_KEY)
    }
    
    //
    // MARK: - Additional Methods
    
    func getLastSession(forActivity activityId: String) -> Session? {
        let predicate = NSPredicate(format: "ANY activity.activityId == %@", activityId)
        let sortDescriptors = [ NSSortDescriptor(key: "startTime", ascending: false) ]
        
        return self.filter(predicate!, sortDescriptors: sortDescriptors)?.first
    }
    
}