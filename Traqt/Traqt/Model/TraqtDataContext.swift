//
//  TraqtDataSource.swift
//  Traqt
//
//  Created by Rafael Veronezi on 9/18/14.
//  Copyright (c) 2014 Syligo. All rights reserved.
//

import CoreData
import CoreDataContext

public class TraqtDataContext : BaseDataContext {

    //
    // MARK: - Public constants
    
    public class var ENTITY_ACTIVITY_NAME: String { return "Activity" }
    public class var ENTITY_ACTIVITY_KEY: String { return "activityId" }
    public class var ENTITY_SESSION_NAME: String { return "Session" }
    public class var ENTITY_SESSION_KEY: String {return "sessionId" }
    
    //
    // MARK: - Singleton
    
    public class var sharedInstance: TraqtDataContext {
        struct Singleton {
            static var instance: TraqtDataContext?
            static var token: dispatch_once_t = 0
        }

        dispatch_once(&Singleton.token) {
            Singleton.instance = TraqtDataContext()
        }
        
        return Singleton.instance!
    }
    
    //
    // MARK: - Entity Data Sources
    
    var activities: ActivityDataSource!
    var sessions: SessionDataSource!
    
    //
    // MARK: - 
    
    init() {
        super.init(resourceName: "Traqt")
        
        if let moc = self.managedObjectContext {
            self.activities = ActivityDataSourceGeneric(managedObjectContext: moc)
            self.sessions = SessionDataSourceGeneric(managedObjectContext: moc)
        }
        self.fillInitialData()
    }
    
    //
    // MARK: - Custom overrides
    
    func fillInitialData() {
        // Verifica se existem atividades
        if self.activities.getAll().count == 0 {
            self.activities.add() {
                $0.name = "Mantras"
                $0.measureRepetitions = true
                $0.enableVibration = true
                $0.measureTime = true
                $0.timeLimit = NSTimeInterval(300)
                $0.repetitions = 10
//                $0.reminders = true
//                $0.reminderWeekDays = WeekDays.Sunday | WeekDays.Monday | WeekDays.Tuesday | WeekDays.Wednesday | WeekDays.Thursday | WeekDays.Friday | WeekDays.Saturday
//                $0.reminderTime =  (NSDate().toIntTime() + 2)
            }
            self.activities.add() {
                $0.name = "Caminhada"
                $0.measureRepetitions = false
                $0.measureTime = true
                $0.timeLimit = NSTimeInterval(5000)
//                $0.reminders = true
//                $0.reminderWeekDays = WeekDays.Sunday | WeekDays.Saturday
//                $0.reminderTime = 480
            }
            self.activities.add() {
                $0.name = "Abdominais"
                $0.measureRepetitions = true
                $0.enableVibration = true
                $0.measureTime = false
                $0.repetitions = 150
                $0.reminders = false
                $0.reminderTime = 1200
            }
//            self.activities.add() {
//                $0.name = "Mantra Noturno"
//                $0.measureRepetitions = true
//                $0.enableVibration = true
//                $0.measureTime = false
//                $0.repetitions = 108
//                $0.reminders = false
//                $0.reminderWeekDays = WeekDays.Monday | WeekDays.Tuesday | WeekDays.Wednesday | WeekDays.Thursday | WeekDays.Friday
//                $0.reminderTime = 1320
//            }
        }
    }
    
}
