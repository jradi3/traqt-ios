//
//  Activity+Helper.swift
//  Traqt
//
//  Created by Rafael Veronezi on 9/18/14.
//  Copyright (c) 2014 Syligo. All rights reserved.
//

import Foundation
import CoreData

/**
    Adds Helper extension methods to the Activity Class
 */
extension Activity {
    
    //
    // MARK: - Overrides
    
    override public func awakeFromInsert() {
        super.awakeFromInsert()
        
        name = ""
        repetitions = 100
        timeLimit = 300
        enableVibration = true
        measureRepetitions = true
        measureTime = true
        reminders = false
    }
    
    //
    // MARK: - Helpers
    
    var reminderWeekDays: WeekDays {
        get {
            return WeekDays(rawValue: self.reminderDays.integerValue)
        }
        set {
            self.reminderDays = newValue.rawValue
        }
    }
    
    func setTimeLimitFromComponents(hours: Int, minutes: Int) {
        self.timeLimit = NSTimeInterval((minutes + (hours * 60)) * 60)
    }
    
    func timeLimitComponents() -> (Int, Int) {
        var (hours, minutes): (Int, Int)
        
        hours = Int(self.timeLimit) / 60 / 60
        minutes = Int(self.timeLimit) / 60 - (hours * 60)
        
        return (hours, minutes)
    }
    
    func addSession(outcome: SessionOutcome, startTime: NSDate, elapsedTime: NSTimeInterval, repetitions: Int) -> Session? {
        let session = NSEntityDescription.insertNewObjectForEntityForName(TraqtDataContext.ENTITY_SESSION_NAME,
            inManagedObjectContext: self.managedObjectContext!) as Session
        session.sessionId = NSUUID().UUIDString
        session.activity = self
        session.sessionOutcome = outcome.rawValue
        session.startTime = startTime
        session.endTime = NSDate()
        session.elapsedTime = elapsedTime
        session.totalRepetitions = repetitions
        
        self.managedObjectContext?.save()
        return session
    }
 
}