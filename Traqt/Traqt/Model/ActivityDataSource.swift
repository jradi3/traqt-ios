//
//  ActivityDataSource.swift
//  Traqt
//
//  Created by Rafael Veronezi on 9/19/14.
//  Copyright (c) 2014 Syligo. All rights reserved.
//

import Foundation
import CoreData
import CoreDataContext

typealias ActivityDataSource = ActivityDataSourceGeneric<AnyObject>

public class ActivityDataSourceGeneric<T> : EntityDataSource<Activity> {
    
    //
    // MARK: - Initialization
    
    init(managedObjectContext: NSManagedObjectContext) {
        super.init(managedObjectContext: managedObjectContext, entityName: TraqtDataContext.ENTITY_ACTIVITY_NAME, entityKeyName: TraqtDataContext.ENTITY_ACTIVITY_KEY)
    }
    
    //
    // MARK: - Custom Overrides

    public override func create() -> Activity {
        let e = super.create()
        e.listOrder = self.getNextListOrder()
        return e
    }
    
    //
    // MARK: - Additional Methods
    
    private func getNextListOrder() -> Int {
        if let lastItem = self.first(sortDescriptors: NSSortDescriptor(key: "listOrder", ascending: false)) {
            return (lastItem.listOrder.integerValue + 1)
        }
        return 0
    }
    
    public func getActivitiesOfDay() -> [Activity]? {
        // Determinar a partir do dia da semana qual a coluna que vai usar na query
        var weekDayColumnName: String
        switch NSDate().weekDay {
        case .Sunday:
            weekDayColumnName = "remindOnSunday"
        case .Monday:
            weekDayColumnName = "remindOnMonday"
        case .Tuesday:
            weekDayColumnName = "remindOnTuesday"
        case .Wednesday:
            weekDayColumnName = "remindOnWednesday"
        case .Thursday:
            weekDayColumnName = "remindOnThursday"
        case .Friday:
            weekDayColumnName = "remindOnFriday"
        case .Saturday:
            weekDayColumnName = "remindOnSaturday"
        }
        
        // Monta o predicate
        let predicate = NSPredicate(format: "%K == YES && reminderTime >= %@", weekDayColumnName, NSNumber(integer: NSDate().toIntTime()))
        let sortDescriptors = [ NSSortDescriptor(key: "reminderTime", ascending: true) ]
        return self.filter(predicate!, sortDescriptors: sortDescriptors)
    }
    
}