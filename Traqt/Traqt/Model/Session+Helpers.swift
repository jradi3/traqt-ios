//
//  Session+Helpers.swift
//  Traqt
//
//  Created by Rafael Veronezi on 10/17/14.
//  Copyright (c) 2014 Syligo. All rights reserved.
//

import Foundation

extension Session {
    
    var startMonth: String {
        let formatter = NSDateFormatter()
        formatter.dateFormat = "MMMM, yyyy"
        
        return formatter.stringFromDate(self.startTime)
    }
    
}