// Playground - noun: a place where people can play

import UIKit

public enum WeekDay : Int {
    case Sunday = 1
    case Monday
    case Tuesday
    case Wednesday
    case Thursday
    case Friday
    case Saturday
}

struct WeekDays : RawOptionSetType, BooleanType {
    
    /**
    Information about some WeekDay
    */
    struct WeekDayInfo {
        
        var longName: String
        var shortName: String
        var value: UInt
        var weekDaysValue: WeekDays? {
            return WeekDays.fromRaw(value)
        }
        var selected: Bool = false
        
        init(weekDay: WeekDay) {
            self.longName = WeekDayInfo.getLongWeekDayName(weekDay)
            self.shortName = WeekDayInfo.getShortWeekDayName(weekDay)
            switch weekDay {
            case .Sunday:
                self.value = WeekDays.Sunday.toRaw()
            case .Monday:
                self.value = WeekDays.Monday.toRaw()
            case .Tuesday:
                self.value = WeekDays.Tuesday.toRaw()
            case .Wednesday:
                self.value = WeekDays.Wednesday.toRaw()
            case .Thursday:
                self.value = WeekDays.Thursday.toRaw()
            case .Friday:
                self.value = WeekDays.Friday.toRaw()
            case .Saturday:
                self.value = WeekDays.Saturday.toRaw()
            }
        }
        
        init(weekDay: WeekDay, isSelected: Bool) {
            self.init(weekDay: weekDay)
            self.selected = isSelected
        }
        
        //
        // MARK: - Static Helpers to Find Localized Week Day Names
        
        static let gregorianCalendar = NSCalendar(calendarIdentifier: NSGregorianCalendar)
        static let longWeekDayNameDateFormatter: NSDateFormatter = {
            var formatter = NSDateFormatter()
            formatter.dateFormat = "EEEE"
            return formatter
            }()
        static func getLongWeekDayName(weekDay: WeekDay) -> String {
            var dateComponents = NSDateComponents()
            dateComponents.weekday = weekDay.toRaw()
            dateComponents.weekdayOrdinal = 1
            dateComponents.year = 2014
            dateComponents.month = 1
            
            var tempDate = gregorianCalendar.dateFromComponents(dateComponents)
            return longWeekDayNameDateFormatter.stringFromDate(tempDate!)
        }
        
        static let shortWeekDayNameDateFormatter: NSDateFormatter = {
            var formatter = NSDateFormatter()
            formatter.dateFormat = "E"
            return formatter
            }()
        static func getShortWeekDayName(weekDay: WeekDay) -> String {
            var dateComponents = NSDateComponents()
            dateComponents.weekday = weekDay.toRaw()
            dateComponents.weekdayOrdinal = 1
            dateComponents.year = 2014
            dateComponents.month = 1
            
            var tempDate = gregorianCalendar?.dateFromComponents(dateComponents)
            return shortWeekDayNameDateFormatter.stringFromDate(tempDate!)
        }
        
    }
    
    //
    // MARK: - Properties
    
    private var value: UInt = 0
    var boolValue: Bool { return value != 0 }
    
    static let weekDayInfos: [WeekDayInfo] = {
        var weekDays = [WeekDayInfo]()
        weekDays.append(WeekDayInfo(weekDay: .Sunday))
        weekDays.append(WeekDayInfo(weekDay: .Monday))
        weekDays.append(WeekDayInfo(weekDay: .Tuesday))
        weekDays.append(WeekDayInfo(weekDay: .Wednesday))
        weekDays.append(WeekDayInfo(weekDay: .Thursday))
        weekDays.append(WeekDayInfo(weekDay: .Friday))
        weekDays.append(WeekDayInfo(weekDay: .Saturday))
        
        return weekDays
        }()
    
    //
    // MARK: - Initializers
    
    init(_ value: UInt) {
        self.value = value
    }
    
    //
    // MARK: - Methods
    
    static func fromMask(raw: UInt) -> WeekDays { return self(raw) }
    static func fromRaw(raw: UInt) -> WeekDays? { return self(raw) }
    func toRaw() -> UInt { return value }
    static var allZeros: WeekDays { return self(0) }
    static func convertFromNilLiteral() -> WeekDays { return self(0) }
    
    func toString() -> String {
        var days: String?
        
        if self & (WeekDays.Sunday | WeekDays.Saturday) {
            days = "Fim de Semana"
        } else if self & (WeekDays.Monday | WeekDays.Tuesday | WeekDays.Wednesday | WeekDays.Thursday | WeekDays.Friday) {
            days = "Dias da Semana"
        } else {
            var isFirst = true
            days = ""
            if self | WeekDays.Sunday {
                days! += WeekDays.weekDayInfos[0].shortName
                isFirst = false
            }
            if self | WeekDays.Monday {
                days! += (isFirst ? "" : " ") + WeekDays.weekDayInfos[1].shortName
                isFirst = false
            }
            if self | WeekDays.Tuesday {
                days! += (isFirst ? "" : " ") + WeekDays.weekDayInfos[2].shortName
                isFirst = false
            }
            if self | WeekDays.Wednesday {
                days! += (isFirst ? "" : " ") + WeekDays.weekDayInfos[3].shortName
                isFirst = false
            }
            if self | WeekDays.Thursday {
                days! += (isFirst ? "" : " ") + WeekDays.weekDayInfos[4].shortName
                isFirst = false
            }
            if self | WeekDays.Friday {
                days! += (isFirst ? "" : " ") + WeekDays.weekDayInfos[5].shortName
                isFirst = false
            }
            if self | WeekDays.Saturday {
                days! += (isFirst ? "" : " ") + WeekDays.weekDayInfos[6].shortName
            }
        }
        
        return days ?? "Nenhum"
        
    }
    
    func asWeekDays() -> [WeekDayInfo] {
        var weekDays = [WeekDayInfo]()
        var isSelected: Bool
        
        isSelected = (self & WeekDays.Sunday) ? true : false
        weekDays.append(WeekDayInfo(weekDay: .Sunday, isSelected: isSelected))
        isSelected = (self & WeekDays.Monday) ? true : false
        weekDays.append(WeekDayInfo(weekDay: .Monday, isSelected: isSelected))
        isSelected = (self & WeekDays.Tuesday) ? true : false
        weekDays.append(WeekDayInfo(weekDay: .Tuesday, isSelected: isSelected))
        isSelected = (self & WeekDays.Wednesday) ? true : false
        weekDays.append(WeekDayInfo(weekDay: .Wednesday, isSelected: isSelected))
        isSelected = (self & WeekDays.Thursday) ? true : false
        weekDays.append(WeekDayInfo(weekDay: .Thursday, isSelected: isSelected))
        isSelected = (self & WeekDays.Friday) ? true : false
        weekDays.append(WeekDayInfo(weekDay: .Friday, isSelected: isSelected))
        isSelected = (self & WeekDays.Saturday) ? true : false
        weekDays.append(WeekDayInfo(weekDay: .Saturday, isSelected: isSelected))
        
        return weekDays
    }
    
    //
    // MARK: - Options
    
    static var None: WeekDays       { return self(0b00000000) }
    static var Sunday: WeekDays     { return self(0b00000001) }
    static var Monday: WeekDays     { return self(0b00000010) }
    static var Tuesday: WeekDays    { return self(0b00000100) }
    static var Wednesday: WeekDays  { return self(0b00001000) }
    static var Thursday: WeekDays   { return self(0b00010000) }
    static var Friday: WeekDays     { return self(0b00100000) }
    static var Saturday: WeekDays   { return self(0b01000000) }
    
    
}

var x = (WeekDays.Sunday | WeekDays.Tuesday)
var y = (WeekDays.Sunday | WeekDays.Monday)
var z = WeekDays.Monday

if x & z {
    print("x")
}

x == x | WeekDays.Sunday
