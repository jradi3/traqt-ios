///
/// TestDateAndTimeHelpers.playground
/// Um Playgroudn para testar a criação das funções de auxílio a trabalho com datas e horas
///

import Foundation

//-----------------------------------------------------------------------------------------
// Extensões de data para cálculo da próxima data de um dia da semana
//-----------------------------------------------------------------------------------------

/**
    Enumera os dias da semana para utilização com o Helper
*/
public enum WeekDay : Int {
    case Sunday = 1
    case Monday
    case Tuesday
    case Wednesday
    case Thursday
    case Friday
    case Saturday
}

/**
    Extensões para o NSDate para obter a próxima data de um dia da semana.
*/
extension NSDate {
    
    /**
        O dia da semana dessa data.
     */
    var weekDay: WeekDay {
        return WeekDay.fromRaw(NSCalendar.currentCalendar().component(.WeekdayCalendarUnit, fromDate: self))!
    }

    /**
        Obtém a próxima data referente ao dia da semana específicado.
        Ex: Qual a próxima segunda a partir de uma determinada data?
        
        :param: weekday O dia da semana para o qual você deseja saber a próxima data.
        :param: date A data de referência para a qual você deseja saber a próxima data.
        :returns: A próxima data referente ao dia da semana desejado.
    */
    class func getNextDate(forWeekDay weekday: WeekDay, forDate date: NSDate) -> NSDate? {
        // Get the current week of year
        let baseComponents = NSCalendar.currentCalendar()
            .components((.YearCalendarUnit | .WeekdayCalendarUnit | .WeekOfYearCalendarUnit), fromDate: date)
        
        // Create a date components for the next week of year on the specified weekday
        let nextDateComponents = NSDateComponents()
        nextDateComponents.year = baseComponents.year
        nextDateComponents.weekOfYear = baseComponents.weekOfYear
        nextDateComponents.weekday = weekday.toRaw()
        
        // Check if the current week day is greater than the next weekday requested
        if baseComponents.weekday > weekday.toRaw() {
            nextDateComponents.weekOfYear++
        }
        
        // Create a calendar to extract date from components
        let calendar = NSCalendar.currentCalendar()
        calendar.firstWeekday = 1
        
        return calendar.dateFromComponents(nextDateComponents)
    }
    
    /**
        Obtém a próxima data referente ao dia da semana específicado.
        Ex: Qual a próxima segunda a partir da data atual?

        :param: weekday O dia da semana para o qual você deseja saber a próxima data.
        :returns: A próxima data referente ao dia da semana desejado.
    */
    func getNextDate(forWeekDay weekday: WeekDay) -> NSDate? {
        return NSDate.getNextDate(forWeekDay: weekday, forDate: self)
    }
    
}

// Testes das extensões
NSDate().getNextDate(forWeekDay: .Sunday)           // Deve apresentar a data do próximo domingo a partir de hoje
NSDate().getNextDate(forWeekDay: .Monday)           // Deve apresentar a data da próxima segunda-feira a partir de hoje
NSDate().getNextDate(forWeekDay: .Tuesday)          // Deve apresentar a data da próxima terça-feira a partir de hoje
NSDate().getNextDate(forWeekDay: .Wednesday)        // Deve apresentar a data da próxima quarta-feira a partir de hoje
NSDate().getNextDate(forWeekDay: .Thursday)         // Deve apresentar a data da próxima quinta-feira a partir de hoje
NSDate().getNextDate(forWeekDay: .Friday)           // Deve apresentar a data da próxima sexta-feira a partir de hoje
NSDate().getNextDate(forWeekDay: .Saturday)         // Deve apresentar a data da próxima sábado a partir de hoje
NSDate().weekDay
print(NSDate().weekDay.toRaw())

//-----------------------------------------------------------------------------------------
// Funções de apoio para serializar e deserializar um horário em um Inteiro
//-----------------------------------------------------------------------------------------

/*
    Serialização de um horário do dia para um inteiro
    A forma mais simples de representar um horário do dia em um inteiro, é armazenando o número de minutos entre a meia noite
    e o horário que desejamos representar. Dessa forma a conta para serialização seria:
    - Cálcular o horário do dia (em formato 24h) por 60
    - Somar o resultado da conta anterior com o número de minutos

    Exemplos:
    - 08:00 = 8 * 60 + 0 = 480
    - 09:30 = 9 * 60 + 30 = 570
    - 14:25 = 14 * 60 + 25 = 865
    - 23:45 = 23 * 60 + 45 = 1425

    De maneira análoga, transcrever a representação inteira de um horário para os componentes de horas e minutos, é obtido através da seguinte conta:
    - divide-se o valor por 60 e a parte inteira é a quantidade de horas
    - obtém-se o resto da divisão do valor por 60 que é a quantidade de minutos

    Exemplos:
    - 480 =
        - 480 / 60 = 8 -> 8 horas
        - 480 % 60 = 0 -> 0 minutos
    - 570 =
        - 570 / 60 = 9.5 -> 9 horas
        - 570 % 60 = 30 -> 30 minutos
    - 865 =
        - 865 / 60 = 14.41667 -> 14 horas
        - 865 % 60 = 25 -> 25 minutos
    - 1425 =
        - 1425 / 60 = 23.75 -> 23 horas
        - 1425 % 60 = 45 -> 45 minutos

    Considerações:
    - O número deve ser positivo
    - O número zero representa "meia-noite"
    - O número não deve ser maior do que 1439, que representa "23:59"
 */

/*
    Extensões para o tipo inteiro para converter uma representação numérica de um horário em outros tipos de objetos.
 */
extension Int {

    static let TIME_MIN = 0
    static let TIME_MAX = 1439
    
    /**
        Extrai os componentes de hora e minuto de uma representação em Inteiro de um horário.
    
        :param: timeInt Valor com a representação em inteiro de um horário.
        :returns: Uma tupla com dois valores inteiros onde o primeiro é a hora e o segundo os minutos.
     */
    static func timeIntToTimeComponents(timeInt: Int) -> (Int, Int)? {
        if timeInt < TIME_MIN || timeInt > TIME_MAX {
            return nil
        }
        
        var hours = timeInt / 60
        var minutes = timeInt % 60
        
        return (hours, minutes)
    }
    
    /**
        Extrai os componentes de hora e minute de uma representação em Inteiro de um horário.

        :param: timeInt Valor com a representação em inteiro de um horário.
        :returns: Uma instância de um NSDateComponents com os valores relativos a hora e os minutos preenchido.
    */
    static func timeIntToDateComponents(timeInt: Int) -> NSDateComponents? {
        if let components = timeIntToTimeComponents(timeInt) {
            let dateComponents = NSDateComponents()
            dateComponents.hour = components.0
            dateComponents.minute = components.1

            return dateComponents
        }
        return nil
    }
    
    /**
        Obtém uma data com o horário configurado conforme os componentes de uma representação em inteiro do horário desejado.
    
        :param: timeInt Valor com a representação em inteiro de um horário.
        :param: baseDate Data para ser usada como base. Dela são aproveitados o dia, mês e ano para compor a data final com o horário desejado.
        :returns: A data usando o horário da representação em inteiro do primeiro parâmetro
     */
    static func timeIntToDate(timeInt: Int, withDate baseDate: NSDate) -> NSDate? {
        if var components = timeIntToDateComponents(timeInt) {
            let baseDateComponents = NSCalendar.currentCalendar().components((.DayCalendarUnit | .MonthCalendarUnit | .YearCalendarUnit), fromDate: baseDate)
            components.day = baseDateComponents.day
            components.month = baseDateComponents.month
            components.year = baseDateComponents.year
            
            return NSCalendar.currentCalendar().dateFromComponents(components)
        }
        
        return nil
    }

    /**
        Retorna a representação em string do horário de uma representação em inteiro do horário desejado.
    
        :param: timeInt Valor com a representação em inteiro de um horário.
        :returns: Uma String representando o horário no formato HH:MM (24h), ou uma string vazia se o horário for inválido.
     */
    static func timeIntToString(timeInt: Int) -> String {
        if let (hour, minute) = Int.timeIntToTimeComponents(timeInt) {
            return "\(hour / 10)\(hour % 10):\(minute / 10)\(minute % 10)"
        }
        
        return ""
    }
    
    /**
        Extrai os componentes de hora e minuto da representação de horário nesse inteiro.
        
        :returns: Uma tupla com dois valores inteiros onde o primeiro é a hora e o segundo os minutos.
    */
    func timeIntToTimeComponents() -> (Int, Int)? {
        return Int.timeIntToTimeComponents(self)
    }

    /**
        Extrai os componentes de hora e minute da representação de horário nesse inteiro.
        
        :returns: Uma instância de um NSDateComponents com os valores relativos a hora e os minutos preenchido.
    */
    func timeIntToDateComponents() -> NSDateComponents? {
        return Int.timeIntToDateComponents(self)
    }
    
    /**
        Obtém uma data com o horário configurado conforme os componentes da representação de horário nesse inteiro.
        
        :param: baseDate Data para ser usada como base. Dela são aproveitados o dia, mês e ano para compor a data final com o horário desejado.
        :returns: A data usando o horário da representação em inteiro do primeiro parâmetro
    */
    func timeIntToDate(baseDate: NSDate) -> NSDate? {
        return Int.timeIntToDate(self, withDate: baseDate)
    }
    
    /**
        Obtém a data atual com o horário configurado conforme os componentes da representação de horário nesse inteiro.
    
        :returns: A data usando o horário da representação em inteiro do primeiro parâmetro
    */
    func timeIntToDate() -> NSDate? {
        return timeIntToDate(NSDate())
    }
    
    /**
        Retorna a representação em string do horário de uma representação de horário nesse inteiro.
        
        :returns: Uma String representando o horário no formato HH:MM (24h), ou uma string vazia se o horário for inválido.
     */
    func timeIntToString() -> String {
        return Int.timeIntToString(self)
    }
    
}

var hour08_00 = 480
var hour09_30 = 570
var hour14_25 = 865
var hour18_20 = 1100
var hour23_45 = 1425
var hourInvalid1 = 240569
var hourInvalid2 = -37

// Testes do método timeIntToDate
hour08_00.timeIntToDate()
hour09_30.timeIntToDate()
hour14_25.timeIntToDate()
hour18_20.timeIntToDate()
hour23_45.timeIntToDate()
hourInvalid1.timeIntToDate()
hourInvalid2.timeIntToDate()

// Testes do método timeIntToDateComponents
let hour08_00comp = hour08_00.timeIntToDateComponents()
hour08_00comp!.hour
hour08_00comp!.minute

let hour09_30comp = hour09_30.timeIntToDateComponents()
hour09_30comp!.hour
hour09_30comp!.minute

let hour14_25comp = hour14_25.timeIntToDateComponents()
hour14_25comp!.hour
hour14_25comp!.minute

let hour18_20comp = hour18_20.timeIntToDateComponents()
hour18_20comp!.hour
hour18_20comp!.minute

let hour23_45comp = hour23_45.timeIntToDateComponents()
hour23_45comp!.hour
hour23_45comp!.minute

hourInvalid1.timeIntToDateComponents()
hourInvalid2.timeIntToDateComponents()

// Testes do método timeIntToDate
hour08_00.timeIntToDate()
hour09_30.timeIntToDate()
hour14_25.timeIntToDate()
hour18_20.timeIntToDate()
hour23_45.timeIntToDate()

// Testes do método timeIntToString
hour08_00.timeIntToString()
hour09_30.timeIntToString()
hour14_25.timeIntToString()
hour18_20.timeIntToString()
hour23_45.timeIntToString()

/*
    Extensões para o NSDateComponents calcular e retornar uma representação numérica inteira das unidades de hora e minutos.
 */
extension NSDateComponents {

    /**
        Retorna a representação em inteiro das unidades de horas e minutos.
    
        :param: dateComponents Objeto do tipo NSDateComponents com as unidades para as quais deseja retornar a representação.
        :returns: A representação em inteiro das unidades.
     */
    class func toIntTime(dateComponents: NSDateComponents) -> Int {
        var intTime = 0
        intTime = dateComponents.hour * 60
        intTime += dateComponents.minute
        return intTime
    }

    /**
        Retorna a representação em inteiro das unidades de horas e minutos.

        :param: dateComponents Objeto do tipo NSDateComponents com as unidades para as quais deseja retornar a representação.
        :returns: A representação em inteiro das unidades.
    */
    func toIntTime() -> Int {
        return NSDateComponents.toIntTime(self)
    }
    
}

/*
    Extensões para um objeto NSDate retornar a representação do seu horário em formato numérico Inteiro.
 */
extension NSDate {

    /**
        Retorna a representação em inteiro do horário que uma data representa.
    
        :param: date Data que deseja extrair o horário.
        :returns: Representação em inteiro do horário da data.
     */
    class func toIntTime(date: NSDate) -> Int {
        let timeComponents = NSCalendar.currentCalendar().components((.HourCalendarUnit | .MinuteCalendarUnit), fromDate: date)
        return timeComponents.toIntTime()
    }
    
    /**
        Retorna a representação em inteiro do horário desta data.
    
        :returns: Representação em inteiro do horário desta data.
     */
    func toIntTime() -> Int {
        return NSDate.toIntTime(self)
    }
    
}

// Testes do método toIntTime
hour08_00comp!.toIntTime()
hour09_30comp!.toIntTime()
hour14_25comp!.toIntTime()
hour18_20comp!.toIntTime()
hour23_45comp!.toIntTime()

let x = (NSDate().toIntTime() + 5)
x.timeIntToDate()

