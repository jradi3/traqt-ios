// Playground - noun: a place where people can play

import Foundation

var gregorian = NSCalendar(calendarIdentifier: NSGregorianCalendar)
var dateComponents = NSDateComponents()
dateComponents.weekday = 1
dateComponents.weekdayOrdinal = 1
dateComponents.year = 2014
dateComponents.month = 1

var tempDate = gregorian.dateFromComponents(dateComponents)
var formatter = NSDateFormatter()
formatter.dateFormat = "EEEE"

var weekdayName = formatter.stringFromDate(tempDate!)