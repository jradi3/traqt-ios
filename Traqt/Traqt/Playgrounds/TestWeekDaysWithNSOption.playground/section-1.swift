// Playground - noun: a place where people can play

import Foundation

///
/// Defines a Struct that allow selection of WeekDays as Options
///
struct WeekDays : RawOptionSetType, BooleanType {
    //
    // MARK: - Properties
    private var value: UInt = 0
    
    //
    // MARK: - Initializers
    init(_ value: UInt) { self.value = value }
    
    var boolValue: Bool { return value != 0 }
    
    static func fromMask(raw: UInt) -> WeekDays { return self(raw) }
    
    static func fromRaw(raw: UInt) -> WeekDays? { return self(raw) }
    
    func toRaw() -> UInt { return value }
    
    static var allZeros: WeekDays { return self(0) }
    
    static func convertFromNilLiteral() -> WeekDays { return self(0) }

    //
    // MARK: - Options
    
    static var None: WeekDays       { return self(0b0000000) }
    static var Sunday: WeekDays     { return self(0b0000001) }
    static var Monday: WeekDays     { return self(0b0000010) }
    static var Tuesday: WeekDays    { return self(0b0000100) }
    static var Wednesday: WeekDays  { return self(0b0001000) }
    static var Thursday: WeekDays   { return self(0b0010000) }
    static var Friday: WeekDays     { return self(0b0100000) }
    static var Saturday: WeekDays   { return self(0b1000000) }
}


let week1: WeekDays = .Sunday | .Tuesday | .Friday
if week1 & .Sunday && week1 & .Tuesday {
    print("Sunday")
    print("Also Tuesday")
}
