//
//  NSDate+IntTime.swift
//  Traqt
//
//  Created by Rafael Veronezi on 9/19/14.
//  Copyright (c) 2014 Syligo. All rights reserved.
//

import Foundation

/*
    Extensões para um objeto NSDate retornar a representação do seu horário em formato numérico Inteiro.
*/
extension NSDate {
    
    /**
        Retorna a representação em inteiro do horário que uma data representa.
        
        :param: date Data que deseja extrair o horário.
        :returns: Representação em inteiro do horário da data.
     */
    class func toIntTime(date: NSDate) -> Int {
        let timeComponents = NSCalendar.currentCalendar().components((.HourCalendarUnit | .MinuteCalendarUnit), fromDate: date)
        return timeComponents.toIntTime()
    }
    
    /**
        Retorna a representação em inteiro do horário desta data.
        
        :returns: Representação em inteiro do horário desta data.
     */
    func toIntTime() -> Int {
        return NSDate.toIntTime(self)
    }
    
}