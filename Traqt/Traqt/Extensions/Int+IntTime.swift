//
//  Int+IntTime.swift
//  Traqt
//
//  Created by Rafael Veronezi on 9/19/14.
//  Copyright (c) 2014 Syligo. All rights reserved.
//

import Foundation

/*
    Extensões para o tipo inteiro para converter uma representação numérica de um horário em outros tipos de objetos.
 */
extension Int {
    
    static let TIME_MIN = 0
    static let TIME_MAX = 1439
    
    /**
        Extrai os componentes de hora e minuto de uma representação em Inteiro de um horário.
        
        :param: timeInt Valor com a representação em inteiro de um horário.
        :returns: Uma tupla com dois valores inteiros onde o primeiro é a hora e o segundo os minutos.
     */
    static func timeIntToTimeComponents(timeInt: Int) -> (Int, Int)? {
        if timeInt < TIME_MIN || timeInt > TIME_MAX {
            return nil
        }
        
        var hours = timeInt / 60
        var minutes = timeInt % 60
        
        return (hours, minutes)
    }
    
    /**
        Extrai os componentes de hora e minute de uma representação em Inteiro de um horário.
        
        :param: timeInt Valor com a representação em inteiro de um horário.
        :returns: Uma instância de um NSDateComponents com os valores relativos a hora e os minutos preenchido.
     */
    static func timeIntToDateComponents(timeInt: Int) -> NSDateComponents? {
        if let components = timeIntToTimeComponents(timeInt) {
            let dateComponents = NSDateComponents()
            dateComponents.hour = components.0
            dateComponents.minute = components.1
            
            return dateComponents
        }
        return nil
    }
    
    /**
        Obtém uma data com o horário configurado conforme os componentes de uma representação em inteiro do horário desejado.
        
        :param: timeInt Valor com a representação em inteiro de um horário.
        :param: baseDate Data para ser usada como base. Dela são aproveitados o dia, mês e ano para compor a data final com o horário desejado.
        :returns: A data usando o horário da representação em inteiro do primeiro parâmetro
     */
    static func timeIntToDate(timeInt: Int, withDate baseDate: NSDate) -> NSDate? {
        if var components = timeIntToDateComponents(timeInt) {
            let baseDateComponents = NSCalendar.currentCalendar().components((.DayCalendarUnit | .MonthCalendarUnit | .YearCalendarUnit), fromDate: baseDate)
            components.day = baseDateComponents.day
            components.month = baseDateComponents.month
            components.year = baseDateComponents.year
            
            return NSCalendar.currentCalendar().dateFromComponents(components)
        }
        
        return nil
    }
    
    /**
        Retorna a representação em string do horário de uma representação em inteiro do horário desejado.
        
        :param: timeInt Valor com a representação em inteiro de um horário.
        :returns: Uma String representando o horário no formato HH:MM (24h), ou uma string vazia se o horário for inválido.
    */
    static func timeIntToString(timeInt: Int) -> String {
        if let (hour, minute) = Int.timeIntToTimeComponents(timeInt) {
            return "\(hour / 10)\(hour % 10):\(minute / 10)\(minute % 10)"
        }
        
        return ""
    }
    
    /**
        Extrai os componentes de hora e minuto da representação de horário nesse inteiro.
        
        :returns: Uma tupla com dois valores inteiros onde o primeiro é a hora e o segundo os minutos.
     */
    func timeIntToTimeComponents() -> (Int, Int)? {
        return Int.timeIntToTimeComponents(self)
    }
    
    /**
        Extrai os componentes de hora e minute da representação de horário nesse inteiro.
        
        :returns: Uma instância de um NSDateComponents com os valores relativos a hora e os minutos preenchido.
     */
    func timeIntToDateComponents() -> NSDateComponents? {
        return Int.timeIntToDateComponents(self)
    }
    
    /**
        Obtém uma data com o horário configurado conforme os componentes da representação de horário nesse inteiro.
        
        :param: baseDate Data para ser usada como base. Dela são aproveitados o dia, mês e ano para compor a data final com o horário desejado.
        :returns: A data usando o horário da representação em inteiro do primeiro parâmetro
     */
    func timeIntToDate(baseDate: NSDate) -> NSDate? {
        return Int.timeIntToDate(self, withDate: baseDate)
    }
    
    /**
        Obtém a data atual com o horário configurado conforme os componentes da representação de horário nesse inteiro.
        
        :returns: A data usando o horário da representação em inteiro do primeiro parâmetro
     */
    func timeIntToDate() -> NSDate? {
        return timeIntToDate(NSDate())
    }
    
    /**
        Retorna a representação em string do horário de uma representação de horário nesse inteiro.
        
        :returns: Uma String representando o horário no formato HH:MM (24h), ou uma string vazia se o horário for inválido.
    */
    func timeIntToString() -> String {
        return Int.timeIntToString(self)
    }
}