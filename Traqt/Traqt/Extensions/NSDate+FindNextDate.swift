//
//  NSDate+Extensions.swift
//  Traqt
//
//  Created by Rafael Veronezi on 9/19/14.
//  Copyright (c) 2014 Syligo. All rights reserved.
//

import Foundation

/**
    Extensões para o NSDate para obter a próxima data de um dia da semana.
 */
extension NSDate {

    /**
        O dia da semana dessa data.
     */
    var weekDay: WeekDay {
        return WeekDay(rawValue: NSCalendar.currentCalendar().component(.WeekdayCalendarUnit, fromDate: self))!
    }
    
    /**
        Obtém a próxima data referente ao dia da semana específicado.
        Ex: Qual a próxima segunda a partir de uma determinada data?
    
        :param: weekday O dia da semana para o qual você deseja saber a próxima data.
        :param: date A data de referência para a qual você deseja saber a próxima data.
        :returns: A próxima data referente ao dia da semana desejado.
     */
    class func getNextDate(forWeekDay weekday: WeekDay, forDate date: NSDate) -> NSDate? {
        // Get the current week of year
        let baseComponents = NSCalendar.currentCalendar()
            .components((.YearCalendarUnit | .WeekdayCalendarUnit | .WeekOfYearCalendarUnit), fromDate: date)
        
        // Create a date components for the next week of year on the specified weekday
        let nextDateComponents = NSDateComponents()
        nextDateComponents.year = baseComponents.year
        nextDateComponents.weekOfYear = baseComponents.weekOfYear
        nextDateComponents.weekday = weekday.rawValue
        
        // Check if the current week day is greater than the next weekday requested
        if baseComponents.weekday > weekday.rawValue {
            nextDateComponents.weekOfYear++
        }
        
        // Create a calendar to extract date from components
        let calendar = NSCalendar.currentCalendar()
        calendar.firstWeekday = 1

        return calendar.dateFromComponents(nextDateComponents)
    }
    
    /**
        Obtém a próxima data referente ao dia da semana específicado.
        Ex: Qual a próxima segunda a partir da data atual?
        
        :param: weekday O dia da semana para o qual você deseja saber a próxima data.
        :returns: A próxima data referente ao dia da semana desejado.
    */
   func getNextDate(forWeekDay weekday: WeekDay) -> NSDate? {
        return NSDate.getNextDate(forWeekDay: weekday, forDate: self)
    }
    
}