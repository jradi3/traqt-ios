//
//  NSDateComponents+IntTime.swift
//  Traqt
//
//  Created by Rafael Veronezi on 9/19/14.
//  Copyright (c) 2014 Syligo. All rights reserved.
//

import Foundation

/*
    Extensões para o NSDateComponents calcular e retornar uma representação numérica inteira das unidades de hora e minutos.
*/
extension NSDateComponents {
    
    /**
        Retorna a representação em inteiro das unidades de horas e minutos.
        
        :param: dateComponents Objeto do tipo NSDateComponents com as unidades para as quais deseja retornar a representação.
        :returns: A representação em inteiro das unidades.
     */
    class func toIntTime(dateComponents: NSDateComponents) -> Int {
        var intTime = 0
        intTime = dateComponents.hour * 60
        intTime += dateComponents.minute
        return intTime
    }
    
    /**
        Retorna a representação em inteiro das unidades de horas e minutos.
        
        :param: dateComponents Objeto do tipo NSDateComponents com as unidades para as quais deseja retornar a representação.
        :returns: A representação em inteiro das unidades.
     */
    func toIntTime() -> Int {
        return NSDateComponents.toIntTime(self)
    }
    
}