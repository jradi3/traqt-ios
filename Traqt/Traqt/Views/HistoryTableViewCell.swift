//
//  HistoryTableViewCell.swift
//  Traqt
//
//  Created by Rafael Veronezi on 10/17/14.
//  Copyright (c) 2014 Syligo. All rights reserved.
//

import UIKit

class HistoryTableViewCell: UITableViewCell {

    //
    // MARK: - Properties
    
    var session: Session? {
        didSet {
            if let s = self.session {
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateStyle = NSDateFormatterStyle.LongStyle
                dateFormatter.timeStyle = NSDateFormatterStyle.MediumStyle
                
                self.outcomeIcon.image = UIImage(named: "outcome_\(s.sessionOutcome)")
                self.dateLabel.text = dateFormatter.stringFromDate(s.startTime)
                self.elapsedTimeLabel.text = NSTimeInterval(s.elapsedTime.floatValue).getFormattedInterval(miliseconds: false)

                if s.activity.measureRepetitions.boolValue {
                    self.repetitionsIcon.hidden = false
                    self.repetitionsLabel.hidden = false
                    self.repetitionsLabel.text = "\(s.totalRepetitions)"
                } else {
                    self.repetitionsIcon.hidden = true
                    self.repetitionsLabel.hidden = true
                }
            }
        }
    }
    
    //
    // MARK: - Outlets
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var elapsedTimeLabel: UILabel!
    @IBOutlet weak var repetitionsIcon: UIImageView!
    @IBOutlet weak var repetitionsLabel: UILabel!
    @IBOutlet weak var outcomeIcon: UIImageView!
    
    //
    // MARK: - Other
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
