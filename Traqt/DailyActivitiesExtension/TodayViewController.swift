//
//  TodayViewController.swift
//  TodayExtension
//
//  Created by Rafael Veronezi on 9/20/14.
//  Copyright (c) 2014 Syligo. All rights reserved.
//

import UIKit
import NotificationCenter

class TodayViewController: UITableViewController, NCWidgetProviding {

    //
    // MARK: - Properties

    let CELL_HEIGHT = CGFloat(44)
    
    var activities = [SharedActivity]()
    var sharedUserDefaults = NSUserDefaults(suiteName: "group.com.syligo.traqt.extensions")
    
    //
    // MARK: - View Lifecycle
    
    func widgetPerformUpdateWithCompletionHandler(completionHandler: ((NCUpdateResult) -> Void)!) {
        self.refreshData()
        if activities.count > 0 {
            completionHandler(NCUpdateResult.NewData)
            
            // Remove o Label informando a ausência de atividades
            self.tableView.tableHeaderView = nil
            self.calculatePreferredContentSize()
            self.tableView.reloadData()
        } else {
            completionHandler(NCUpdateResult.NoData)
            
            // Adiciona uma Label informando que não há atividades
            let label = UILabel()
            label.textColor = UIColor.whiteColor()
            label.text = "Nenhuma atividade pendente."
            label.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: label.intrinsicContentSize().height + 20)
            self.tableView.tableHeaderView = label
            self.preferredContentSize = CGSize(width: 0, height: label.intrinsicContentSize().height + 20)
        }
    }
    
    //
    // MARK: - Support Methods
    
    func calculatePreferredContentSize() {
        var totalHeight = CGFloat(self.activities.count) * CELL_HEIGHT + self.tableView.sectionFooterHeight
        self.preferredContentSize = CGSize(width: 0, height: totalHeight)
    }
    
    func refreshData() {
        activities.removeAll(keepCapacity: true)
        if let sharedActivities = sharedUserDefaults?.objectForKey("SharedActivities") as? [NSDictionary] {
            let refTime = NSDate().toIntTime()
            for activityDictionary in sharedActivities {
                let activity = SharedActivity(info: activityDictionary)
                let weekDays = WeekDays(rawValue: activity.daysOfWeek.integerValue)
                
                // Filter by time of the day and day of the week
                if activity.timeOfDay.integerValue >= refTime && (NSDate().weekDay.asWeekDays() & weekDays) {
                    activities.append(activity)
                }
            }
        }
    }

    //
    // MARK: - UITableViewDataSource
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return activities.count
    }
    
    //
    // MARK: - UITableViewDelegate
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return CELL_HEIGHT
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("ActivityCell", forIndexPath: indexPath) as UITableViewCell
        
        cell.textLabel?.text = activities[indexPath.row].name
        cell.detailTextLabel?.text = activities[indexPath.row].timeOfDayDesc
        
        return UITableViewCell()
    }
    
}
