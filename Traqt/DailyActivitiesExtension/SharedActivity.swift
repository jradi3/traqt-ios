//
//  SharedActivity.swift
//  Traqt
//
//  Created by Rafael Veronezi on 9/23/14.
//  Copyright (c) 2014 Syligo. All rights reserved.
//

import Foundation

public let kSharedActivityFieldId = "ActivityId"
public let kSharedActivityFieldName = "Name"
public let kSharedActivityFieldDaysOfWeek = "DaysOfWeek"
public let kSharedActivityFieldTimeOfDay = "TimeOfDay"

/**
    A Activity to be 
 */
public class SharedActivity: NSObject {
    
    //
    // MARK: - Properties
    
    var activityId: String
    var name: String
    var daysOfWeek: NSNumber
    var timeOfDay: NSNumber
    var timeOfDayDesc: String {
        return self.timeOfDay.integerValue.timeIntToString()
    }
    
    //
    // MARK: - Initializers
    
    init(activityId: String, name: String, daysOfWeek: Int, timeOfDay: Int) {
        self.activityId = activityId
        self.name = name
        self.daysOfWeek = daysOfWeek
        self.timeOfDay = timeOfDay
        super.init()
    }
    
    init(info: NSDictionary) {
        self.activityId = info[kSharedActivityFieldId] as String
        self.name = info[kSharedActivityFieldName] as String
        self.daysOfWeek = info[kSharedActivityFieldDaysOfWeek] as NSNumber
        self.timeOfDay = info[kSharedActivityFieldTimeOfDay] as NSNumber
        super.init()
    }
    
}
