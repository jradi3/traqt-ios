//
//  CoreDataContext.h
//  CoreDataContext
//
//  Created by Rafael Veronezi on 9/30/14.
//  Copyright (c) 2014 Syligo. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for CoreDataContext.
FOUNDATION_EXPORT double CoreDataContextVersionNumber;

//! Project version string for CoreDataContext.
FOUNDATION_EXPORT const unsigned char CoreDataContextVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CoreDataContext/PublicHeader.h>


