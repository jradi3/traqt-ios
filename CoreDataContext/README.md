CoreDataContext
===============

CoreDataContext is a Cocoa Touch Framework written in Swift to allow fast integration of Core Data into your iOS projects, and easy data operations on defined entities.
