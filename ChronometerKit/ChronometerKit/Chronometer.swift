//
//  Chronometer.swift
//  Chronometer
//
//  Created by Rafael Veronezi on 8/21/14.
//  Copyright (c) 2014 Syligo. All rights reserved.
//

import Foundation

public typealias ChronometerUpdateBlock = ((NSTimeInterval, NSTimeInterval?) -> ())?
public typealias ChronometerCompletedBlock = (() -> ())?

///
/// Defines a Chronometer class, that can be used to keep track of time.
/// The class uses NSTimer object to keep the refresh pace, which means 
///  that this class is not best swited for precision, since refresh may take
///  longer than specified, based on the available resources.
///
/// This class is not thread safe.
///
public class Chronometer: NSObject {

    //
    // MARK: - Private Properties
    
    private var startTime = NSTimeInterval(0)
    private var accumulatedTime = NSTimeInterval(0)
    private var elapsedSinceLastRefresh = NSTimeInterval(0)
    private var timer = NSTimer()
    
    //
    // MARK: - Public Properties
    
    public var elapsedTime: NSTimeInterval {
        return elapsedSinceLastRefresh + accumulatedTime
    }
    
    /// The Time Interval to refresh the chronometer. The default is 0.5 seconds
    public var refreshInterval = NSTimeInterval(0.5)

    /// Determines a time limit for this Chronometer
    public var timeLimit: NSTimeInterval?
    
    /// Optional Block that gets called on each refresh of the specified time interval.
    /// If this class needs to update UI, make sure that the UI class are made in the
    ///  main thread, or dispatched into it.
    public var updateBlock: ChronometerUpdateBlock
    
    /// Optional Block that gets called when the chronometer reach its limit time
    public var completedBlock: ChronometerCompletedBlock
    
    //
    // MARK: - Initializers
    
    ///
    /// A convenience initializer that allow specifying the refresh interval
    /// - parameter refreshInterval: The desired refresh interval
    ///
    public convenience init(refreshInterval: NSTimeInterval) {
        self.init()
        self.refreshInterval = refreshInterval
    }
    
    ///
    /// A convenience initializer that allow specifying the refesh interval and update block
    /// - parameter refreshInterval: The desired refresh interval
    /// - parameter updateBlock: The update block to be called on each refresh
    ///
    public convenience init(refreshInterval: NSTimeInterval, updateBlock: (NSTimeInterval, NSTimeInterval?) -> ()) {
        self.init()
        self.refreshInterval = refreshInterval
        self.updateBlock = updateBlock
    }
    
    //
    // MARK: - Internal Methods
    
    ///
    /// Refresh the timer, calling the update block to notify the tracker about the new value.
    ///
    func refreshTime() {
        // Calculate the new time
        let refreshTime = NSDate.timeIntervalSinceReferenceDate()
        self.elapsedSinceLastRefresh = (refreshTime - startTime)
        
        // Calculates the remaining time if applicable
        var remainingTime: NSTimeInterval? = nil
        if self.timeLimit != nil {
            remainingTime = self.timeLimit! - elapsedTime
        }

        // If an update block is specified, then call it
        self.updateBlock?(elapsedTime, remainingTime)
        
        // If the chronometer is complete, then call the block and
        if let limit = self.timeLimit {
            if self.elapsedTime >= limit {
                self.stop()
                self.completedBlock?()
            }
        }
    }
    
    //
    // MARK: - Public Methods
    
    ///
    /// Set a time limit for this chronometer
    ///
    public func setLimit(timeLimit: NSTimeInterval, withCompletionBlock completedBlock: () -> ()) {
        self.timeLimit = timeLimit
        self.completedBlock = completedBlock
    }
    
    ///
    /// Start the execution of the Cronometer.
    /// Start will take place using accumulated time from the last session. 
    /// If the Cronometer is running the call will be ignored.
    ///
    public func start() {
        if !timer.valid {
            // Create a new timer
            timer = NSTimer.scheduledTimerWithTimeInterval(self.refreshInterval,
                target: self,
                selector: "refreshTime",
                userInfo: nil,
                repeats: true)

            // Set the base date
            startTime = NSDate.timeIntervalSinceReferenceDate()
        }
    }
    
    ///
    /// Stops the execution of the Cronometer.
    /// Keeps the accumulated value, in order to allow pausing of the chronometer, and
    ///  to keep "elapsedTime" property value available for the user to keep track.
    ///
    public func stop() {
        timer.invalidate()
        accumulatedTime = elapsedTime
        elapsedSinceLastRefresh = 0
    }
    
    ///
    /// Resets the Cronometer.
    /// This method will stop chronometer if it's running. This is necessary since 
    ///  the class is not thread safe.
    ///
    public func reset() {
        timer.invalidate()
        elapsedSinceLastRefresh = 0
        accumulatedTime = 0
    }
    
}
