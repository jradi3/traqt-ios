//
//  ChronometerKit.h
//  ChronometerKit
//
//  Created by Rafael Veronezi on 8/24/14.
//  Copyright (c) 2014 Syligo. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for ChronometerKit.
FOUNDATION_EXPORT double ChronometerKitVersionNumber;

//! Project version string for ChronometerKit.
FOUNDATION_EXPORT const unsigned char ChronometerKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ChronometerKit/PublicHeader.h>


